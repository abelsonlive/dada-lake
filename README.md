# `dada-lake`

a `dada-lake` is an asset, metadata, and graph store, designed to ingest, transform, remix, and relate files of all kinds. Its API is built to serve the textures, images, videos, 3D objects, and sounds found on [`fifteen.pm`](https://fifteen.pm) and [`globally.ltd`](https://globally.ltd). in the long term, we hope it will serve as a sort of creative database for all of our projects, and a source of inspiration iself.

## how is a dada-lake organized? 

a `dada-lake` is organized into a series of `entity_types` which can be related to each other. at its core is the `file` entity, which represents an individual file of any format. archive files (eg `zip`, `tar.gz`, etc) are also supported.


### files

`files` can have a `file_type` of `audio`, `image`, `video`, `data`, `code`, `doc`, `bundle` or simply `raw`. these types are inferred upon ingestion.
- `audio` files are `mp3`, `aiff`, `midi`, `flac`, etc. _(supported)_
- `image` files are `jpg`, `png`, `tiff`, `svg`, etc. _(supported)_
- `video` files are `mp4`, `webm`, `mov`, etc. _(supported)_
- `model` files are `gltf`, `obj`, `fbx`, etc. _(in progress)_
- `data` files are `csv`, `json`, `parquest`, `shp`, etc. _(in progress)_
- `code` files are `py`, `js`, `sh`, `html`, `css`, etc.  _(in progress)_
- `doc` files are `docx`, `txt`, `ppt`, `md`, etc.  _(in progress)_
- `bundle` files are `zip`, `tar.gz`, etc _(supported)_
    - the contents of `bundle` files are extracked and nested under the original archive file.
    - these contents can also be extracted as their own files, with the relationship between the parent and child retained. more on this below re: `macros`.
- `raw` files are those that don't fall into the above categories or whose types cannot be inferred.

### fields

for each `file` you can create a series of `fields` to associate with it. these `fields` can have their own `type`, representing the data value (eg `int`, `text`, `float`, etc) stored in the `field`. On ingestion, input data are validated against these `type`. You can also associate `fields` with any other `entity_type`. in this way, `fields` enable dada-lake's schema to be specified on a per-instance basis, or grow large to fit various use cases within a single instance. a dada-lake can be small: just a database and directory on your local machine, or a large cluster of databases and distributed file stores across many users and many machines. 

### folders + desktops

`files` can be related to `folders` and/or `desktops`. in each case, the `file` can have an associated `position` in the `folder` and/or `desktop`. `folders` can similarly be nested under `desktops`, with their own distinct `position`. `folders` and `desktops` can contain any combination of `files`. `fields` can also be associated with `file_folder` and `folder_desktop` relations, enabling the potential of relation-specific attributes. 

### themes + tags

every entity in the `dada-lake` can also have an associated `theme` and set of `tags`. A `theme` represents visual components associated with an `entity_type`. for a `image file`, this might include a thumbnail of that image, or a series of dominant colors contained within it. for a `folder`, this might include a representative emoji or a cover image. like other entities, `themes` can also have `fields`, so their schema is similarly flexible.

`tags` are global and can exist in a many-to-many relationship with any other `entity_type` (including other `tags`). `tags` allow you to easily create groups of related `entity_types`. for instance, we tag all `entity_types` related to `fifteen.pm` in order to keep track of all the assets related to that project. that being said, some of those `entity_types` can also be related to `globally.ltd`, a different `tag`.

### edges + graphs

finally, `edges` represent arbitrary relationships between any two `entity_types` (including relationships between the same `entity_type`). `entity_types` connect via `edges` through the `graph` model, which connects an `edge_id`, a `from_entity_type`, a `from_id`, a `to_entity_type`, and a `to_id`. Similar to `fields`, this data model allows us to create different `edges` for different `file_types` and `entity_types`. a `graph` can also have `attributes` which, like `fields`, can have a custom data type. this allows us to dynamically create weighted, bi-directional edges between different `entity_types`.

## how is data transformed?

**NOTE**: Most of this is still a WIP, though the idea is close to completion.

### macros

`entity_types` can be modified via `macros`. `macros` are simple python functions which accept an `entity_type` and/or `params` and do something with them. Within a `macro`, you can make calls to the dada-lake api, allowing you to programatically create new `files`, `folders`, `desktops`, etc. Examples of such macros might be:
    - run a api search for files and assign the results to a folder 
    - run an api search for all `image files`, perform an analysis, and create a `graph` relationship to all other `image files` using the `edge` of `image_similarity` 
    - run a api search for `audio files` with the `ext` of `mp3`, convert them to `wav`, and save new `files`, with a `graph` relationship added between the source file and the converted file using the `edge` of `file_format`.
    - run a api search for `audio files`, extract features via `essentia` and add them as `fields`. 
    - run a api search for `model files` with the `ext` of `gltf` and perform optimizations on the geometries
    - accept a search query and scrape a webpage of images and upload them into the `dada-lake` under a new folder which is named according to the search query. 
    - ...

### tasks

`macros` which operate on similar `entity_types` can be nested together under a `task`. similar to how `files` and `folders` are related, `macros` can have an associated `position` within a `task`. when a `task` is `chained`, it will run the `macro` in `position` `1` and pass its results to the `macro` in `position` `2`, and so on and so forth. `params` for each `macro` within a task can be predefined or set at runtime. 

### jobs + hooks

`macros` and `tasks` can be associated with either `jobs` or `hooks`. `jobs` are scheduled operations that run a list of `tasks` and/or `macros` at a specific time. `hooks` are operations that occur after a given `action` on an `entity_type` has ocurred. for instance, when a `file` is `created`, we might trigger  a `macro` to extract additional metadata from the `file`  and upload the results back to the database as `fields`.

## how is data queried?

data is queried via the a RESTful JSON API. each `entity_type` shares a similar search interface. full documentation (as well as an interactive api console) are available at [http://localhost:3030/docs] after starting a local server. (more on this below)

### filter strings

`dada-lake` employs a DSL for constructing search filter statements we call "filter strings". filter strings are comprised of three elements: a field name, an operation, and a value. for instance, the filter string `name:lk:brian%` would translate to the SQL statement `WHERE name like 'brian%'`. `bpm:bt:100,120` would translate to `WHERE bpm BETWEEN 100 and 120` filters can be combined togeter via an `or` or `and` logic to build increasingly complex queries.

TK...

## how are files stored? 

When files are imported into a `dada-lake` they are first stored `locally` on any drive accessible to the API server (for instance, if running locally, on an external hard drive connected to your computer, or if on a cloud server, some network accessible drive. this parameter is set in the[.env](.env.sample) file). common metadata is extracted for all imported `files`, including a `check_sum`, which is used to create a unique `partition` for each file. underneath this partition, separate `version` partitions are created for each update to a `file`. in addition a `latest` version is set for each update.

an example of a `local` url might be: 
- `/Users/joora/e=file/t=audio/s=track/x=mp3/y=20/m=5/d=22/h=10/i=1/v=dklfkasdflkdsajfalsdf-20052210/Screen Shot 2020-05-19 at 9.10.58 AM.png`
- here `e` is the `entity_type`, `t` is the `file_type`, `x` is the file `ext`, `y`, `m`, `d`, and `h` are the entity's created year, month, date, and hour, respectively. and `v` is the unique version identifier, consisting of the file `check_sum` and a `udpated_at` time.

in addition to this file, a `.dada` file is also created. a `.dada` file is a `json.gz` encoded representation of metadata associated with that file. this includes all assoicated `fields`, `folders`, `tags`, etc. `.dada` files live in the same directory as their associated files. for instance, the `.dada` path to the url above would be: `/Users/joora/e=file/t=audio/s=track/x=mp3/y=20/m=5/d=22/h=10/i=1/v=dklfkasdflkdsajfalsdf-20052210/Screen Shot 2020-05-19 at 9.10.58 AM.dada`

each `dada-lake` can also be configured to distribute assets `globally`. this entails pushing a file currently stored locally to an amazon s3 bucket with a similar directory structure. the only difference here, however, is that all files pushed to `s3` are automatically `gzipped` (unless they are already compressed (eg `mp4` files)). each `file` can be set to `is_public`, which will make this url publically accessible on the web. the file store can optionally configured with a cloudfront distribution, as well, and each update to a file will trigger an invalidation on its `latest` url (as well as the current `version` url).

finally, instead of setting `s3` as your `global` store, you can set another `dada-lake` instance as the upload endpoint. in this configuration, you would upload from a local `dada-lake` instance (or even another deployed `dada-lake`) and this endpoint would then upload files to s3. 


## docker build

You can start up an API cluster using [`docker-compose`](https://docs.docker.com/compose/).

first, copy [`.env.sample`](.env.sample) to `.env` and add your credentials. the most importat are the AWS access key and secret.

next run this command.:

```
docker-compose up -d 
```

You should now be able to access the following resources:

- [http://localhost:3031/docs](http://localhost:3031/docs)
    * API Documentation, Powered by [Swagger](https://swagger.io/)
- [http://localhost:3031/spec.json](http://localhost:3031/spec.json)
    * An [OpenAPI](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md) Specification
- [http://localhost:3031/users?api_key=dev](http://localhost:3031/users?api_key=dev)
    * An example API endpoint

## local development

to set up a local _MAC OS X_ dev environment, first install the `brew` dependencies:

```
brew bundle
```

Next create a virtual environment with using your tool of choice and install an editable version of the library:

```
pip3 install -e
```
Next, copy [`.env.sample`](.env.sample) to `.env` and add your credentials. the most importat are the AWS access key and secret.

You should now be able to run the common [`Makefile`](./Makefile) commands. 

Most make commands accept the argument `env={}`. this value can either be `test`, `dev`, `prod`, or `docker`. 
each of these environments load different app configurations found in [`dada/config`](dada/config).

- `make db-upgrade`: run alembic migrations 
- `make db-migrate`: record a migration and create an updated [db schema diagram](migrations/db-schema.pdf)
- `make db-create-defaults`: create defaults as defined in [`dada/config/models/`](dada/config/models)
- `make serv-flask`: run a local development server 
- `make serv-gunicorn`: run a production gunicorn server 
- `make shell`: open a flask shell inside the application context with access to common models.

### start a local server 
- to start a local server, first open another shell and ensure that `redis-server` is running. 
- in another tab, start a `celery` helper by running `make celery-helper env={}`
- in another shell, start the api server by runnng `make serv-flask env={}`

you should be able to navigate to the api docs at [http://localhost:3030/docs]

### tests
- many tests require no other resources and can be run standalone via `make test_one t=path/to/test.py`
- tests that interact with the API and require `celery`  should be run by first starting a celery helper using the `env` of `test`:
  * `make celery-helper env=test`
- at which point you can run the full test suite via `make test` 

## TODO

### PRIORITIES
- [ ] Finishing writing out tests for all core api methods (`fields`, `files`, `users`, `desktops`, etc.)
- [ ] fix docker setup and finish modernizing to new `docker-compose` features
- [x] bundling!
- [x] versioning
- [x] `gltd_file` module for enforcing consistent metdata and partitioned urls for files in varied contexts (cli / api / macros / etc)
- [ ] public / private controls file controls and tests for this functionality.
- [ ] basic cli for uploading / downloading / searching files 
- [ ] macro running via cli + api 
- [ ] task running via cli + api 
- [ ] tests for tagging 
- [ ] tests for adding relationships
- [ ] tests for relationship filtering
- [ ] tests for removing relationships
- [ ] rename the project
- [ ] job scheduling

### Feature Ideas
- [ ] Move to ltree model for file-file + tag-tag + folder-folder + desktop-desktop relationships
    - https://www.postgresql.org/docs/current/ltree.html
    - https://kite.com/blog/python/sqlalchemy/
- [ ] Build out `Site` model, which should:
    - have an associated list of `files`
    - have a dedicated s3 bucket 
    - have a dedicated cloudfront distribution
    - have a deicated route 53 dns entry
    - be publishable via api  / hook, so than when the underlying files change, the site updates. 
    - the idea here is to bye able to heuristically generate and deploy sites. 
    -  maybe a "folder" could just be site?. it would be better if we could have a recursive directory tree i guee.
- [ ] Write job scheduler 
- [ ] Job Scheduling / Status checking.
- [ ] GRAPH QUERIES: https://www.postgresql.org/docs/9.5/queries-with.html
  - [ ] Include field metadata in file.get 
  - [ ] Automatically create FileFieldView and query that instead of doing join.
  - [ ] `.gldb` file in bundle folders to determine parsing
- [ ] Add `data` file type which creates queryable, joinable tables in athena

### Macro/Task/Job Features

- [x] Task parameters
- [ ] Command-line execution
- [ ] Metaprogramming to build up list of Macros
- [ ] Job Scheduling 
- [ ] Hooks
- TASKS TO IMPLEMENT
  - [x] Rename + Update ID3 tags
  - [ ] Implement bulk-editing API
  - [ ] Youtube DL 
    - https://github.com/ytdl-org/youtube-dl/blob/master/README.md#embedding-youtube-dl
  - [ ] Ingestion
    - [ ] Rekordbox
    - [ ] Itunes
    - [ ] Archive
    - [ ] Soundcloud
    - [ ] Youtube
    - [ ] Bandcamp
    - [ ] Mixcloud
    - [ ] Beatport
  - [ ] De-duping via fuzzy-string matching
