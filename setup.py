import os
from setuptools import setup, find_packages

reqs = os.path.abspath(os.path.join(os.path.dirname(__file__), "requirements.txt"))
with open(reqs) as f:
    install_requires = [req.strip().split("==")[0] for req in f]

config = {
    "name": "dada-lake",
    "version": "0.1",
    "packages": find_packages(),
    "install_requires": install_requires,
    "author": "gltd",
    "author_email": "dev@globally.ltd",
    "description": "API and Filesystem for dada",
    "url": "http://globally.ltd",
    "install_requires": install_requires,
    "entry_points": {
        "console_scripts": [
            "dada=dada.cli.main:run",
            "dada-admin=dada.cli.admin:run",
            "dada-helper=dada.cli.helper:run",
        ]
    },
}

setup(**config)
