SELECT
    'CREATE DATABASE sickdb'
WHERE
    NOT EXISTS (
        SELECT
        FROM
            pg_database
        WHERE
            datname = 'sickdb'
    );
