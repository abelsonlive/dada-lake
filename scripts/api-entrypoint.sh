#!/bin/sh

set -x

echo "Waiting for postgres..."
while ! nc -z postgres 5432; do
  sleep 10
done
echo "PostgreSQL started..."
echo "Running Migrations..."

make db-init env=docker 
make db-upgrade env=docker 
echo "Installing Defaults..."
make db-create-defaults env=docker 
echo "Starting API..."
make serv-gunicorn env=docker
