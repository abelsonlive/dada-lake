import logging

from dada.queue.core import celery
from dada import gldb_file
from dada.models.core import db_execute
from dada.utils import path, serde

CELERY_GLDB_LOGGER = logging.getLogger()


@celery.task
def save(filepath, **kwargs):
    """
    Upload a file to s3 as a background task
    """
    CELERY_GLDB_LOGGER.info(f"[Q.gldb_file.save] saving {filepath}")
    gl = gldb_file.load(filepath, **kwargs)
    gl.save_locally()
    gl.serve_globally()
    # HACK: insert extracted bundle field here
    # because we cant cross import
    # from this file to dada.models.file
    if gl.db.file_type == "bundle":
        query = """
            UPDATE file
            SET fields = '{fields}'
            WHERE id = {id}
        """.format(
            id=gl.db.get("id"), fields=serde.obj_to_json(gl.db.fields)
        )
        db_execute(query)

    path.remove(filepath)
