"""
TODO: Add custom Marshmallow Fields / Validators / Docs Here.
"""


Fields_Request_Name_Docs = """
The name of this **Field**.

Usually in ```snake_case```
"""
Fields_Request_Type_Docs = f"""
The schema type of this **Field**.

_One or more of_:

{", ".join(TYPE_NAMES)}
"""

Fields_Request_Accepts_FileType_Docs = f"""
The file type(s) this field accepts

_One or more of_:

{", ".join(settings.FILE_DEFAULTS_FILE_TYPES + ['all'])}
"""

Fields_Request_Accepts_FileSubType_Docs = f"""
The file subtype(s) this field accepts

_One or more of_:

{", ".join(settings.FILE_DEFAULTS_FILE_SUBTYPES + ['all'])}
"""
