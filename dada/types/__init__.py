from dada.types.lib import (
    # core objects
    GLDBType,
    GLDBTypes,
    # helper functions
    NewVal,
    NewType as NewPy,
)

# shorthand access to our type library
T = GLDBTypes()
