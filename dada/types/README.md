# dada-lake Types

Types are scalar, column values. By creating a custom type library, unifying type definitions for sqlalchemy, marshmallow, python, faker, and flask, we can avoid repetitious type definitions and potential data degradation. 