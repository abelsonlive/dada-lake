from flask import Blueprint
from dada.models.job import Job
from dada.models.tag_join_table import JobTag
from dada.models.api_response import JobResponse

from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Jobs API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

JOBS_APP = crud_blueprint_from_model(
    blueprint=Blueprint("jobs", __name__),
    core_model=Job,
    base_response_schema=JobResponse,
    doc_tags=["jobs"],
    relationships=[JobTag],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)

# ///////////////////////
# TODO run-based api
# ///////////////////////
