from flask import Blueprint
from dada.models.graph import Graph
from dada.models.api_response import GraphResponse

from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Graph API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

GRAPHS_APP = crud_blueprint_from_model(
    blueprint=Blueprint("graph", __name__),
    core_model=Graph,
    base_response_schema=GraphResponse,
    doc_tags=["graph"],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)


# ///////////////
# TODO: Traversals? or leave that for macros ?
# ///////////////
