from flask import Blueprint
from dada.models.task import Task
from dada.models.task_hook import TaskHook
from dada.models.task_job import TaskJob
from dada.models.tag_join_table import TaskTag
from dada.models.api_response import TagResponse
from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Sites API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

TASKS_APP = crud_blueprint_from_model(
    blueprint=Blueprint("tasks", __name__),
    core_model=Task,
    base_response_schema=TagResponse,
    doc_tags=["tasks"],
    relationships=[TaskTag, TaskHook, TaskJob],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)
