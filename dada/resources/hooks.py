from flask import Blueprint
from dada.models.hook import Hook
from dada.models.tag_join_table import HookTag
from dada.models.api_response import HookResponse

from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Hooks API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

HOOKS_APP = crud_blueprint_from_model(
    blueprint=Blueprint("hooks", __name__),
    core_model=Hook,
    base_response_schema=HookResponse,
    doc_tags=["hooks"],
    relationships=[HookTag],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)
