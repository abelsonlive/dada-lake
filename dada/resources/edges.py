from flask import Blueprint
from dada.models.edge import Edge
from dada.models.tag_join_table import EdgeTag
from dada.models.api_response import EdgeResponse

from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Edges API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

EDGES_APP = crud_blueprint_from_model(
    blueprint=Blueprint("edges", __name__),
    core_model=Edge,
    base_response_schema=EdgeResponse,
    doc_tags=["edges"],
    relationships=[EdgeTag],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)


# ///////////////
# TODO: Traversals? or leave that for macros ?
# ///////////////
