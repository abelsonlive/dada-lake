from flask import Blueprint
from dada.models.macro import Macro
from dada.models.macro_hook import MacroHook
from dada.models.macro_task import MacroTask
from dada.models.macro_job import MacroJob
from dada.models.tag_join_table import MacroTag
from dada.models.api_response import MacroResponse

from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Macros API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

MACROS_APP = crud_blueprint_from_model(
    blueprint=Blueprint("macros", __name__),
    core_model=Macro,
    base_response_schema=MacroResponse,
    doc_tags=["macros"],
    relationships=[MacroTag, MacroTask, MacroHook, MacroJob],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)

# ///////////////////////
# TODO run-based api
# ///////////////////////
