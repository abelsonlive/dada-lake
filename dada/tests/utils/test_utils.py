import os
import logging
import unittest
from datetime import datetime

from dada.tests.core import BaseTest

from dada.utils import archive, dates, text, path, serde
from dada.config import settings

TEST_LOGGER = logging.getLogger()


class UtilTests(BaseTest):
    def test_archive_extraction(self):
        files = archive.extract_all(self.get_fixture("Archive.zip"))
        assert set([f.split("/")[-1] for f in files]) == set(["file1.txt", "file2.txt"])

    def test_dates_is_field_date(self):
        assert dates.is_field_date("created_at")
        assert dates.is_field_date("release_date")
        assert dates.is_field_date("created_datetime")
        assert dates.is_field_date("created")
        files = archive.extract_all(self.get_fixture("Archive.zip"))
        assert set([f.split("/")[-1] for f in files]) == set(["file1.txt", "file2.txt"])

    def test_text_to_list(self):
        assert text.to_list("1,2") == ["1", "2"]
        assert text.to_list("1+2") == ["1", "2"]
        assert text.to_list("[1,2]") == [1, 2]  # json
        assert text.to_list("(1,2)") == ["1", "2"]

    def test_text_to_bool(self):
        assert text.to_bool("y")
        assert text.to_bool("yes")
        assert text.to_bool("TRUE")
        assert text.to_bool("1")
        assert text.to_bool("t")
        assert not text.to_bool("n")
        assert not text.to_bool("f")
        assert not text.to_bool("0")
        assert not text.to_bool("FALSE")
        assert text.to_bool("none") is None

    def test_text_is_null(self):
        assert text.is_null("na")
        assert text.is_null("NA")
        assert text.is_null("<na>")
        assert text.is_null("NaN")
        assert text.is_null("null")
        assert text.is_null("NONE")
        assert text.is_null("-")
        assert not text.is_null("okay")
        assert text.is_null(None)

    def test_text_is_int(self):
        assert text.is_int("1")
        assert text.is_int("12345")
        assert not text.is_int("1234.56")
        assert not text.is_int("NaN")

    def test_text_is_not_int(self):
        assert text.is_not_int("absdfa")
        assert text.is_not_int("1dasf45")
        assert not text.is_not_int("1234")
        assert not text.is_not_int("1")

    # //////////////
    # path
    # /////////////

    def test_path_check_sum(self):
        FP = self.get_fixture("eril-brinh2.mp3")
        assert path.get_check_sum(FP) == path.exec(f"md5 -q {FP}").stdout.strip()

    def test_path_get_location(self):
        assert path.get_location("s3://foo/bar") == "s3_ext"
        assert path.get_location(f"s3://{settings.S3_BUCKET}/bar") == "s3_int"
        assert path.get_location(f"/Users/folders") == "loc_ext"
        assert path.get_location(f"{settings.LOCAL_DIR}/foo/bar") == "loc_int"
        assert path.get_location(f"foo/bar") == "loc_rel"
        assert path.get_location(f"https://web.com") == "web"
        assert path.get_location(f"web.com/file.txt") == "web"

    def test_path_get_created_at(self):
        assert isinstance(
            path.get_updated_at(self.get_fixture("Archive.zip")), datetime
        )

    def test_path_get_updated_at(self):
        assert isinstance(
            path.get_updated_at(self.get_fixture("Archive.zip")), datetime
        )

    def test_get_ext_and_mimetype(self):
        assert path.get_ext_and_mimetype(self.get_fixture("space-time-motion.mp3")) == (
            "mp3",
            "audio/mpeg",
        )
        assert path.get_ext_and_mimetype(self.get_fixture("Archive.zip")) == (
            "zip",
            "application/zip",
        )

    def test_get_ext_with_gz(self):
        assert path.get_ext("file.json.gz") == "json.gz"
        assert path.get_ext("file.mp3.gz") == "mp3.gz"
        assert path.get_ext("file.gz") == "gz"

    def test_get_gzipped_ungzipped_names(self):
        assert path.get_gzipped_name("file.mp3") == "file.mp3.gz"
        assert path.get_gzipped_name("file.mp3.gz") == "file.mp3.gz"
        assert path.get_ungzipped_name("FilE.mp3.GZ") == "FilE.mp3"
        assert path.get_ungzipped_name("the.gzip.file.mp3.gz") == "the.gzip.file.mp3"

    def test_path_get_name_removes_gz(self):
        assert path.get_name("file.json.gz") == "file"
        assert path.get_name("file.json") == "file"
        assert path.get_name("json.file.json.gz") == "json.file"

    # //////////////
    # serde
    # /////////////

    def test_serde_obj_from_json(self):
        """
        Since we use obj_from_json to also validate json objects
        it should accept dictionaries and lists as inputs and simple pass them through
        """
        o1 = serde.json_to_obj(["foo", "bar"])
        o2 = serde.json_to_obj('["foo","bar"]')
        assert o1 == o2

    def test_serde_obj_from_json_invalid_json(self):
        try:
            serde.json_to_obj("['foo']")
        except:
            assert True
        else:
            assert False


if __name__ == "__main__":
    unittest.main()
