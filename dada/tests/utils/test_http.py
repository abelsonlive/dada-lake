import os
import logging
from dada.tests.core import BaseTest

from dada.utils import http, path
from dada.config import settings

TEST_LOGGER = logging.getLogger()


class UtilTests(BaseTest):
    def test_http_download_file(self):
        local_path = http.download_file("http://example.com/")
        assert path.exists(local_path)


if __name__ == "__main__":
    unittest.main()
