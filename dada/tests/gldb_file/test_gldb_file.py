import time
from datetime import datetime

from dada import gldb_file
from dada.utils import path
from dada.tests.core import S3Test
from dada.models.core import s3conn


class GldbFileTests(S3Test):
    def test_load_local_external(self):

        url = self.get_fixture("Archive.zip")
        gl = gldb_file.load(url)
        # test file writing.
        gl.save_locally()

        # test for metadata existence
        assert gl.db.id is None
        assert gl.db.check_sum == "1641279865bcaafea6c14604edb896e9"
        assert gl.db.file_type == "bundle"
        assert gl.db.file_subtype == "raw"
        assert gl.db.ext == "zip"
        assert gl.db.size == 926
        assert gl.db.mimetype == "application/zip"
        assert isinstance(gl.db.created_at, datetime)
        assert isinstance(gl.db.updated_at, datetime)
        assert gl.urls.input.file.latest == url

        # test for file existence
        assert path.exists(gl.urls.local.file.latest)
        assert path.exists(gl.urls.local.file.version)
        assert path.get_check_sum(gl.urls.local.file.latest) == path.get_check_sum(
            gl.urls.input.file.latest
        )

    def test_partition_url(self):
        url = "s3://abelsonlive/gldb/e=file/t=audio/s=track/x=mp3/y=20/m=5/d=22/h=10/i=1/v=dklfkasdflkdsajfalsdf-20052210/Screen Shot 2020-05-19 at 9.10.58 AM.png"
        gl = gldb_file.load(url)
        assert gl.is_part_url
        assert gl.db["entity_type"] == "file"
        assert gl.db["file_type"] == "audio"
        assert gl.db["file_subtype"] == "track"
        assert gl.db["ext"] == "png"
        assert gl.db["created_year2"] == 20
        assert gl.db["check_sum"] == "dklfkasdflkdsajfalsdf"
        assert gl.db["version"] == "dklfkasdflkdsajfalsdf-20052210"
        assert gl.db["updated_at"] == datetime(2020, 5, 22, 10, 0)
        assert gl.db["created_at"] == datetime(2020, 5, 22, 10, 0)
        assert gl.db["id"] == 1

    def test_save_s3_ext_file_locally(self):
        url = "s3://abelsonlive/gldb/e=file/t=audio/s=track/x=mp3/y=20/m=5/d=22/h=10/i=1/v=dklfkasdflkdsajfalsdf-20052210/Screen Shot 2020-05-19 at 9.10.58 AM.png"
        gl = gldb_file.load(url)
        gl.save_locally()
        assert path.exists(gl.urls.local.file.latest)
        assert path.exists(gl.urls.local.file.version)
        assert path.exists(gl.urls.local.gldb.latest)
        assert path.exists(gl.urls.local.gldb.version)

    def test_save_s3_ext_file_globally(self):
        url = "s3://abelsonlive/gldb/e=file/t=audio/s=track/x=mp3/y=20/m=5/d=22/h=10/i=1/v=dklfkasdflkdsajfalsdf-20052210/Screen Shot 2020-05-19 at 9.10.58 AM.png"
        gl = gldb_file.load(url)
        gl.save_locally()
        gl.serve_globally()
        assert s3conn.exists(gl.urls.s3.file.latest)
        assert s3conn.exists(gl.urls.s3.file.version)
        assert s3conn.exists(gl.urls.s3.gldb.latest)
        assert s3conn.exists(gl.urls.s3.gldb.version)

    def test_bundle_extraction(self):
        url = self.get_fixture("Archive.zip")
        gl = gldb_file.load(url)
        gl.save_locally()
        assert len(gl.local_bundle_urls) == 2
        assert path.exists(gl.dirs.local_bundle.latest + "file1.txt")
        assert path.exists(gl.dirs.local_bundle.latest + "file2.txt")
        gl.serve_globally()
        assert len(gl.s3_bundle_urls) == 2
        print(gl.dirs.s3_bundle.latest)
        assert s3conn.exists(gl.dirs.s3_bundle.latest + "file1.txt")
        assert s3conn.exists(gl.dirs.s3_bundle.latest + "file2.txt")
        assert len(gl.db.fields["bundle_contents"]) == 2
        assert set(gl.db.fields["bundle_contents"]) == set(["file1.txt", "file2.txt"])

    def test_local_mp3_extraction(self):
        url = self.get_fixture("eril-brinh2.mp3")
        gl = gldb_file.load(url)
        gl.ensure_gldb(url)
        assert gl.db.ext == "mp3"
        assert gl.db.file_type == "audio"
        gl.save_locally()
        assert gl.db.ext == "mp3"
        assert gl.db.file_type == "audio"
        gl.serve_globally()
        assert gl.db.ext == "mp3"
        assert gl.db.file_type == "audio"
