from dada.utils import path
from dada.gldb_file.core import S3ExtFile, S3IntFile, LocExtFile, LocIntFile, WebFile

FILE_LOCATIONS = {
    "s3_ext": S3ExtFile,
    "s3_int": S3IntFile,
    "loc_ext": LocExtFile,
    "loc_int": LocIntFile,
    "web": WebFile,
}


def load(url, location=None, **kwargs):
    """
    Load a file into the local store
    """

    # determine location / prepare url
    if not location:
        location = path.get_location(url)
        if location == "loc_rel":
            url = path.get_full(url)
            location = "loc_ext"
    return FILE_LOCATIONS.get(location)(url, location, **kwargs)
