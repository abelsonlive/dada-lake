"""
Functions for inferring schema from a file.
"""
from collections import OrderedDict

from dada.types import T
from dada.config import settings
from dada.utils import path, dates


#
# file metadata
#


def get_gldb_type_from_file_metadata(**file_metadata):
    """"""
    ext = file_metadata.get("ext")
    mt = file_metadata.get("mimetype")
    type = None
    if ext is not None:
        type = get_type_from_ext(ext)
    if not type and mt is not None:
        type = get_type_from_mimetype(mt)
    return type


# gldb schema extraction
def get_type_from_ext(ext: T.ext.py_optional) -> T.file_type.py_optional:
    """
    Infer the dada type from the extension
    """
    if not ext:
        return None
    for type, extensions in settings.FILE_VALID_TYPE_EXT_MIMETYPE.items():
        if ext in list(extensions.keys()):
            return type
    return None


def get_type_from_mimetype(mimetype: T.mimetype.py_optional) -> T.file_type.py_optional:
    """
    Infer the dada type from the extension
    """
    if not mimetype:
        return None
    for typ, mimetypes in settings.FILE_VALID_TYPE_MIMETYPE_EXT.items():
        if mimetype in list(mimetypes.keys()):
            return typ
    return None


def get_default_subtype_for_type(
    type: T.file_type.py,
) -> T.file_subtype.py_optional:

    return settings.FILE_DEFAULTS_DEFAULT_FILE_SUBTYPE_FOR_FILE_TYPE.get(
        type, settings.FILE_DEFAULTS_DEFAULT_FILE_SUBTYPE
    )


DATE_ATTR = ["year", "month", "day", "hour"]
DATE_FIELDS = ["created_at", "updated_at"]


def get_date_attributes_from_file_metadata(**file_meta) -> dict:
    data = {}
    for field in DATE_FIELDS:
        val = file_meta.get(field, None)
        if val is None:
            continue
        if isinstance(val, str):
            val = dates.from_string(val)
        for attr in DATE_ATTR:
            name = field.replace("_at", f"_{attr}")
            attr_value = getattr(val, attr)
            if attr_value is None:
                continue
            data[name] = attr_value
            if name.endswith("_year"):
                data[name + "2"] = str(attr_value)[2:]
    return data
