# """
# A .gldb file is two files:
#     - 1: any old file and
#     - 2: a `.gldb` file containing json data including the file's metadata as well as gldb-specific fields (including source, theme, folders, tags, macro, task, etc)

# A .gldb file can exist in five places:
#     - loc_ext (somewhere on your loc machine)
#     - loc_int (saved in the loc .gldb/ cache)
#     - s3_ext (on a s3 bucket other than the one managed by this gldb instance)
#     - s3_int (on the s3 bucket managed by this gldb instance)
#     - web (somewhere on a publically accessible website)
import gzip
import logging
from typing import Optional

from botocore.exceptions import ClientError

from dada.utils import dates
from dada.types import T
from dada.types.base import SerializableObject
from dada.utils import etc, path, serde, text, http, archive
from dada.models.core import s3conn
from dada.gldb_file import schema, partition
from dada.config import settings


GZ_COMPRESSION_EXCLUDES = frozenset(
    ["mp4", "webm"] + settings.BUNDLE_DEFAULTS_VALID_EXT
)
GLDB_LOGGER = logging.getLogger()


class GldbData(etc.AttrDict):
    pass


class GldbUrlSet(etc.AttrDict):
    pass


class GldbFile(SerializableObject):

    # default metadata

    ensure_check_sum = False
    local_dir = path.get_full(settings.LOCAL_DIR)
    s3_bucket = s3conn

    def __init__(
        self,
        url: Optional[T.url.py] = None,
        location: Optional[str] = None,
        ensure_check_sum: Optional[bool] = None,
        **gldb,
    ):
        # ensure local directory endslash
        if not self.local_dir.endswith("/"):
            self.local_dir += "/"

        # determine location
        self.location = location
        self.ensure_check_sum = ensure_check_sum or self.ensure_check_sum

        # if this is a url based loader, do its thing.
        self.url = None
        self.gldb_url = None
        self.db = GldbData({"entity_type": "file"})

        if url is not None:
            self.url = url
            # for fetching dada data files from other dada-lake instances
            self.gldb_url = f"{path.get_dir(self.url)}/{path.get_name(self.url)}.gldb"

            # ensure name, slug, and extension are set by default
            gldb.setdefault("file_name", path.get_name(url))
            gldb.setdefault("slug", text.get_slug(path.get_name(url)))
            gldb.setdefault("ext", path.get_ext(url))

            # determine compression
            self.is_gz = self.url.endswith(".gz")

            # extract partitioned url gldb data
            self.is_part_url = partition.is_partition_url(self.url)
            if self.is_part_url:
                self.db.update(partition.extract(self.url))

        # update gldb metadata with overrides
        self.db.update(gldb)

    @property
    def id_partition(self):
        """ the partition for all versions of this file (eg e=file/t=audio/s=track/x=mp3/i=1/) """
        return partition.create(**self.db)

    @property
    def version_partition(self):
        """ the partition for current version of this file (eg e=file/t=audio/s=track/x=mp3/i=1/v=3820740923482-fldjkasfd) """
        return self.id_partition + partition.version(**self.db) + "/"

    @property
    def latest_partition(self):
        """ the partition for the latest version of this file (eg e=file/t=audio/s=track/x=mp3/i=1/v=latest) """
        return f"{self.id_partition}v=latest/"

    @property
    def file_name(self):
        return f"{self.db.slug}.{self.db.ext}"

    @property
    def file_name_gz(self):
        if self.db.ext not in GZ_COMPRESSION_EXCLUDES:
            return f"{self.file_name}.gz"
        return self.file_name

    @property
    def gldb_file_name(self):
        return f"{self.db.slug}.gldb.gz"

    @property
    def dirs(self):
        return GldbUrlSet(
            {
                "local": GldbUrlSet(
                    {
                        "latest": self.local_dir + self.latest_partition,
                        "version": self.local_dir + self.version_partition,
                    }
                ),
                "local_bundle": GldbUrlSet(
                    {"latest": self.local_dir + self.version_partition + "bundle/"}
                ),
                "s3": GldbUrlSet(
                    {
                        "latest": self.s3_bucket.s3_prefix + self.latest_partition,
                        "version": self.s3_bucket.s3_prefix + self.version_partition,
                    }
                ),
                "s3_bundle": GldbUrlSet(
                    {
                        "latest": self.s3_bucket.s3_prefix
                        + self.latest_partition
                        + "bundle/",
                    }
                ),
            }
        )

    @property
    def urls(self):
        """
        store uncompressed files locally listening / usage / processing
        store compressed files on s3 for performance / savings
        """
        return GldbUrlSet(
            {
                "input": GldbUrlSet(
                    {
                        "file": GldbUrlSet({"latest": self.url}),
                        "gldb": GldbUrlSet({"latest": self.gldb_url}),
                    }
                ),
                "local": GldbUrlSet(
                    {
                        "file": GldbUrlSet(
                            {
                                "latest": self.dirs.local.latest + self.file_name,
                                "version": self.dirs.local.version + self.file_name,
                            }
                        ),
                        "gldb": GldbUrlSet(
                            {
                                "latest": self.dirs.local.latest + self.gldb_file_name,
                                "version": self.dirs.local.version
                                + self.gldb_file_name,
                            }
                        ),
                    }
                ),
                "s3": GldbUrlSet(
                    {
                        "file": GldbUrlSet(
                            {
                                "latest": self.dirs.s3.latest + self.file_name_gz,
                                "version": self.dirs.s3.version + self.file_name_gz,
                            }
                        ),
                        "gldb": GldbUrlSet(
                            {
                                "latest": self.dirs.s3.latest + self.gldb_file_name,
                                "version": self.dirs.s3.version + self.gldb_file_name,
                            }
                        ),
                    }
                ),
            }
        )

    @property
    def local_bundle_urls(self):
        """ a list of bundle urls locally """
        return [
            path.join(self.dirs.local_bundle.latest, f)
            for f in self.db.get("fields", {}).get("bundle_contents", [])
        ]

    @property
    def s3_bundle_urls(self):
        """ a list of bundle urls on s3 """
        return [
            path.join(self.dirs.s3_bundle.latest, f)
            for f in self.db.get("fields", {}).get("bundle_contents", [])
        ]

    def download_file(self, from_url, to_url) -> None:
        """
        The download file method is responsible for downloading a url into the local store,
        locating any associated `.gldb` files
        and updating internal gldb data before we construct the partitions
        """
        raise NotImplementedError("A GLDBFile must implement a `download_file` method")

    def ensure_gldb(self, tmp_file):
        """
        ensure that crucial metadata is set for every file, no matter the source.
        """
        # ensure created at
        if self.db.get("created_at", None) is None:
            self.db["created_at"] = path.get_created_at(tmp_file)
        elif isinstance(self.db.get("created_at"), str):
            self.db["created_at"] = dates.from_string(self.db.get("created_at"))

        # ensure updated at
        if self.db.get("updated_at", None) is None:
            self.db["updated_at"] = path.get_updated_at(tmp_file)

        elif isinstance(self.db.get("updated_at"), str):
            self.db["updated_at"] = dates.from_string(self.db.get("updated_at"))

        # ensure created / updated hour
        if self.db.get("created_hour", None) is None:
            self.db.update(schema.get_date_attributes_from_file_metadata(**self.db))

        if self.db.get("check_sum", None) is None:
            self.db["check_sum"] = path.get_check_sum(tmp_file)

        if self.db.get("size", None) is None:
            self.db["size"] = path.get_size(tmp_file)

        if self.db.get("mimetype", None) is None:
            ext, mt = path.get_ext_and_mimetype(tmp_file)
            self.db["mimetype"] = mt
            if self.db.get("ext", None) is None:
                self.db["ext"] = ext

        if self.db.get("file_type", None) is None:
            self.db["file_type"] = schema.get_gldb_type_from_file_metadata(**self.db)

            if self.db.get("file_subtype", None) is None:
                self.db["file_subtype"] = schema.get_default_subtype_for_type(
                    self.db.get("file_type")
                )
        if self.db.get("id", None) is None:
            self.db["id"] = None

    def save_locally(self):
        """ save a file and metadata into the local store. """
        self.internal_set_timer_start("save_locally")
        tmp_file = path.join(path.get_tempdir(), self.file_name)
        self.download_file(self.url, tmp_file)
        if self.is_gz:
            tmp_file = self._extract_gzip_file(tmp_file)

        # okay, now, no matter the source we should have an uncompressed
        # file which we can extract gldb data from and import, let's go to work

        # ensure core metadata
        self.ensure_gldb(tmp_file)

        # write the file locally
        self._save_file(tmp_file)

        # extract bundles
        if self.db.file_type == "bundle":
            self._extract_bundle()

        # write gldb
        self._save_gldb()
        self.internal_set_timer_end("save_locally")
        GLDB_LOGGER.debug(
            f"[gldb-import] job took {self.internal_timers['save_locally']['human']}"
        )

    def serve_globally(self):
        """ write local versions of a file and gldb data to s3 """
        self.internal_set_timer_start("serve_globally")

        # gzip the file before serving it
        is_tmp_file = False
        if self.db.ext not in GZ_COMPRESSION_EXCLUDES:
            is_tmp_file = True
            to_upload = path.join(path.get_tempdir(), self.file_name_gz)
            serde.file_to_gz(self.urls.local.file.version, to_upload)

        else:
            to_upload = self.urls.local.file.version

        self._serve_file(to_upload)
        self._serve_gldb()
        if self.db.file_type == "bundle":
            self._serve_bundle()

        # cleanup
        if is_tmp_file:
            path.remove(to_upload)
        self.internal_set_timer_end("serve_globally")
        GLDB_LOGGER.debug(
            f"[gldb-serve] job took {self.internal_timers['serve_globally']['human']}"
        )

    def _extract_gzip_file(self, tmp_file):
        """ if input file is a gzip, extract it """
        # remove 'gz' from ext in gldb data, thereby updating the url properties
        if self.db.ext is not None:
            self.db.ext = path.get_ungzipped_name(self.db.ext)

        # ungzip to a new tempfile
        tmp_file_gz = tmp_file
        tmp_file_ungzipped = path.get_ungzipped_name(tmp_file)
        serde.gz_to_file(tmp_file_gz, tmp_file_ungzipped)

        # update tmpfile var with new information
        tmp_file = tmp_file_ungzipped

        # remove the gzipped file
        path.remove(tmp_file_gz)
        return tmp_file

    def _save_file(self, tmp_file):
        """ write a file to to the local store """
        # copy tmpfile to local version
        GLDB_LOGGER.debug(
            f"[gldb-import] Importing {tmp_file} to {self.urls.local.file.version}"
        )
        path.copy(tmp_file, self.urls.local.file.version)

        GLDB_LOGGER.debug(
            f"[gldb-import] backing up {self.urls.local.file.version} to {self.urls.local.file.latest}"
        )
        path.copy(self.urls.local.file.version, self.urls.local.file.latest)
        path.remove(tmp_file)

    def _save_gldb(self):
        """ write gldb metadata to the local store """
        GLDB_LOGGER.debug(
            f"[gldb-import] Writing gldb data to {self.urls.local.gldb.version}"
        )
        with open(self.urls.local.gldb.version, "wb") as f:
            f.write(self.to_jsongz())

        GLDB_LOGGER.debug(
            f"[gldb-import] backing up gldb data to {self.urls.local.gldb.latest}"
        )
        path.copy(self.urls.local.gldb.version, self.urls.local.gldb.latest)

    def _extract_bundle(self):
        """
        in the instance that this file is a "bundle"
        we'll want to extract its contents and also add those to the file store.
        however, these files will not be versioned themselves and will instead be considered
        part of the original "bundle" version, and nested under an associated "bundle" directory
        in the file's `id_partition`

        each time a new version of a `bundle` file is imported, its contents will be added to the local store
        as well as to s3. this way we can serve the contents of a bundle together.
        for instance, a bundle might constitute an entire website. in this case, we'd
        want to retain the directory structure of the bundle so as to ensure that
        relative path references work appropriately
        """
        self.internal_set_timer_start("bundle_extract")
        GLDB_LOGGER.debug(
            f"extracting bundle contents from {self.urls.local.file.latest} to {self.dirs.local_bundle.latest}"
        )
        path.make_dir(self.dirs.local_bundle.latest)

        extracted_files = list(
            archive.extract_all(
                self.urls.local.file.latest,
                self.dirs.local_bundle.latest,
                backend="auto" if self.db.ext != "zip" else "zip",
            )
        )
        GLDB_LOGGER.debug(f"[gldb-import] {extracted_files}")

        rel_files = [
            f.replace(self.dirs.local_bundle.latest, "") for f in extracted_files
        ]

        # set the bundle contents as a field
        if "fields" not in self.db:
            self.db["fields"] = {}
        self.db["fields"]["bundle_contents"] = rel_files
        self.internal_set_timer_end("bundle_extract")
        GLDB_LOGGER.debug(
            f"[gldb-import] bundle extraction took {self.internal_timers['bundle_extract']['human']}"
        )

    def _serve_file(self, to_upload):
        """ write file to s3 """
        GLDB_LOGGER.debug(
            f"[gldb-serve] uploading {to_upload} to {self.urls.s3.file.version}"
        )

        self.s3_bucket.upload(to_upload, self.urls.s3.file.version)
        GLDB_LOGGER.debug(
            f"[gldb-serve] copying {self.urls.s3.file.version} to {self.urls.s3.file.latest}"
        )
        self.s3_bucket.copy(self.urls.s3.file.version, self.urls.s3.file.latest)

    def _serve_gldb(self):
        """ write gldb to s3 """
        GLDB_LOGGER.debug(
            f"[gldb-serve] uploading gldb data from {self.urls.local.gldb.version} to {self.urls.local.gldb.latest}"
        )
        self.s3_bucket.upload(self.urls.local.gldb.version, self.urls.s3.gldb.version)

        GLDB_LOGGER.debug(
            f"[gldb-serve] copying gldb data from {self.urls.s3.gldb.version} to {self.urls.s3.gldb.latest}"
        )
        self.s3_bucket.copy(self.urls.s3.gldb.version, self.urls.s3.gldb.latest)

    def _serve_bundle(self):
        """ write bundle contents to s3 """
        for from_url, to_url in zip(self.local_bundle_urls, self.s3_bundle_urls):
            GLDB_LOGGER.debug(
                f"[gldb-serve] copying bundle file {from_url} to {to_url}"
            )
            self.s3_bucket.upload(from_url, to_url)

    # helpers for checking for existence in various locations

    def version_exists_locally(self) -> bool:
        return path.exists(self.urls.local.file.version)

    def latest_exists_locally(self) -> bool:
        return path.exists(self.urls.local.file.latest)

    def version_exists_globally(self) -> bool:
        return self.s3_bucket.exists(self.urls.s3.file.version)

    def latest_exists_globally(self) -> bool:
        return self.s3_bucket.exists(self.urls.s3.file.latest)

    def exists_locally(self) -> bool:
        return self.latest_exists_locally() and self.version_exists_locally()

    def exists_globally(self) -> bool:
        return self.version_exists_globally() and self.latest_exists_globally()

    # serializable data representation
    def to_dict(self) -> dict:
        return self.db


class S3ExtFile(GldbFile):
    def download_file(self, from_url, to_url):
        try:
            gldb_data = serde.jsongz_to_obj(
                self.s3_bucket.external_get_contents(self.gldb_url)
            )
            self.db.update(gldb_data)
        except ClientError as e:
            GLDB_LOGGER.warning(
                f"WARNING could not fetch [s3_ext] gldb_url {self.gldb_url} because of: {e}"
            )

        self.s3_bucket.external_download(from_url, to_url)


class S3IntFile(GldbFile):
    def download_file(self, from_url, to_url):
        if self.s3_bucket.exists(self.gldb_url):
            GLDB_LOGGER.debug(
                f"[gldb-s3_int-download_file] fetching gldb data from {self.db.url}"
            )
            gldb_data = serde.jsongz_to_obj(self.s3_bucket.get_contents(self.gldb_url))
            self.db.update(gldb_data)
        self.s3_bucket.download(from_url, to_url)


class LocExtFile(GldbFile):
    location = "loc_ext"

    def download_file(self, from_url, to_url):
        if path.exists(self.gldb_url):
            GLDB_LOGGER.debug(
                f"[gldb-{self.location}-download_file] fetching gldb data from {self.db.url}"
            )
            gldb_data = serde.jsongz_to_obj(open(self.gldb_url).read())
            self.db.update(gldb_data)
        path.copy(from_url, to_url)


class LocIntFile(LocExtFile):
    location = "loc_int"


class WebFile(GldbFile):

    session = http.get_session()

    def download_file(self, from_url, to_url):
        if http.exists(self.gldb_url):
            GLDB_LOGGER.debug(f"[gldb-web_ext] fetching gldb data from {self.gldb_url}")
            r = self.session.get(self.gldb_url)
            r.raise_for_status()
            gldb_data = serde.jsongz_to_obj(r.content)
            self.meta.update(gldb_data)
        http.download_file(from_url, to_url)
