# Partitions
#
from datetime import datetime
from collections import OrderedDict

from dada.types import T


PARTITION_SCHEMA = OrderedDict(
    {
        "entity_type": "e",
        "file_type": "t",
        "file_subtype": "s",
        "ext": "x",
        "created_year2": "y",
        "created_month": "m",
        "created_day": "d",
        "created_hour": "h",
        "id": "i",
    }
)
PARTITION_ABRR_TO_ATTR = {v: k for k, v in PARTITION_SCHEMA.items()}

VERSION_SCHEMA = ["check_sum", "updated_at"]
VERSION_DATE_FORMAT = "%y%m%d%H"


def is_partition_url(url) -> bool:
    """ checks if a url is a gldb partitioned url"""
    return all([f"/{k}=" in url for k in PARTITION_ABRR_TO_ATTR.keys()])


def version(**kwargs) -> T.partition.py:
    """ create a version string given kwargs """
    parts = []
    for attr in VERSION_SCHEMA:
        p = kwargs.get(attr, None)
        if not p:
            p = ""
        if isinstance(p, datetime):
            p = p.strftime(VERSION_DATE_FORMAT)
        parts.append(p)
    return f'v={"-".join(parts)}'


def create(**kwargs) -> T.partition.py:
    """ create a partition given kwargs"""
    part_string = ""
    for part, abbr in PARTITION_SCHEMA.items():
        pval = kwargs.get(part, None)
        if pval is None:
            pval = ""
        part_string += f"{abbr}={pval}/"
    return part_string


#
# partiton extraction
#


def extract(url) -> dict:
    """ extract schema from a partitioned url """
    d = {}
    if not is_partition_url(url):
        return d
    to_search = dict(
        list({"v": "version"}.items()) + list(PARTITION_ABRR_TO_ATTR.items())
    )
    for abbr, attr in to_search.items():

        parts = url.split(f"/{abbr}=")
        if len(parts) < 2:
            continue
        if attr != "version":
            value = parts[1].split("/")[0]
            if attr.startswith("created_") or attr == "id":
                value = int(value)
            d[attr] = value

        else:
            version = parts[1].split("/")[0]
            d["version"] = version
            for sub_attr, sub_val in zip(VERSION_SCHEMA, version.split("-")):
                d[sub_attr] = sub_val

    return extract_dates(d)


def extract_dates(d) -> dict:
    # format dates
    if "created_year2" in d:
        d["created_at"] = datetime(
            **{
                "year": int(f"20{d.get('created_year2', '20')}"),
                "month": int(d.get("created_month", "1")),
                "day": int(d.get("created_day", "1")),
                "hour": int(d.get("created_hour", "0")),
                "minute": int(d.get("created_minute", "0")),
            }
        )

    if "updated_at" in d.keys():
        d["updated_at"] = datetime.strptime(d["updated_at"], VERSION_DATE_FORMAT)
    return d
