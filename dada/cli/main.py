"""
The core CLI Client that uses the API exclusively
"""
from dada.types import T


def run():
    print(T.names)


if __name__ == "__main__":
    run()
