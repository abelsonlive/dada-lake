"""
A separate API Program for running admin tasks in the app context
"""

import logging

from flask_migrate import Migrate
import click

# core objects
from dada.factory import create_app
from dada.queue.core import celery
from dada.models.core import db, db_execute, s3conn, rdsconn

# Type Helpers
from dada.types import T
from dada.types.flt import FilterString

# pass in full utils library to shell context
from dada import utils

# MODELS
from dada.models.cache import Cache
from dada.models.desktop import Desktop
from dada.models.edge import Edge
from dada.models.field import Field, FieldCache
from dada.models.file import File
from dada.models.folder import Folder
from dada.models.graph import Graph
from dada.models.hook import Hook
from dada.models.job import Job
from dada.models.macro import Macro
from dada.models.source import Source
from dada.models.tag import Tag
from dada.models.task import Task
from dada.models.user import User
from dada.macros.sql.file_denorm import FileDenorm

# etc.
from dada.cli.core import LINE_HEADER
from dada.config import settings

# initialize app / plugins
app = create_app(celery=celery)
migrate = Migrate(app, db)
CLI_ADMIN_LOGGER = logging.getLogger()

# initialize the shell contenxt
@app.shell_context_processor
def make_shell_context():
    return dict(
        # core connections
        app=app,
        db=db,
        db_execute=db_execute,
        s3conn=s3conn,
        rdsconn=rdsconn,
        utils=utils,
        Cache=Cache,
        Edge=Edge,
        Field=Field,
        FieldCache=FieldCache,
        File=File,
        Graph=Graph,
        Hook=Hook,
        Job=Job,
        Macro=Macro,
        Source=Source,
        Tag=Tag,
        Task=Task,
        User=User,
    )


# ///////////////////
# ADMIN COMMANDS
# ///////////////////


@app.cli.command(help="Initialize the Database")
def db_init():
    """"""
    db.configure_mappers()  # for sqlalchemy searchable
    db.create_all(bind=None, app=app)
    db.session.commit()


@app.cli.command(help="Create built-in defaults in the database")
def db_create_defaults():
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default users...")
    User.create_defaults()
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default tags...")
    Tag.create_defaults(user_id=1)
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default fields...")
    Field.create_defaults(tag_id=[1, 2])
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default jobs...")
    Job.create_defaults(tag_id=[2])
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default edges...")
    Edge.create_defaults(tag_id=[1])
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default hooks...")
    Hook.create_defaults(tag_id=[2])
    ###
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default macros...")
    Macro.create_defaults()
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default desktops...")
    Desktop.create_defaults(user_id=1, tag_id=[2])
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default folders...")
    Folder.create_defaults(user_id=1, tag_id=[1, 2])
    CLI_ADMIN_LOGGER.info(f"{LINE_HEADER}creating default files...")
    File.create_defaults(user_id=1, tag_id=[1, 2])
    # CLI_ADMIN_LOGGER.info("Constructing file-field search view...")
    # FileDenorm().run()
    db.session.commit()


@app.cli.command(help="[WARNING] Drop the databsase")
def db_drop():
    if click.prompt(
        f"{LINE_HEADER}Are you sure you want to lose all your data?", bool=True
    ):
        db.drop_all(bind=None)


def run():
    """
    Run the admin CLI.
    """
    app.cli()
