from typing import Optional, Dict, Any

"""
Utilities from processing, converting, and transforming videos
TODO: 
- Remove reliance on ffmpeg_streaming
"""
# ///////////////////
# Imports
# ///////////////////

from ffmpeg_streaming import FFProbe

from dada.utils import path


# ///////////////////
# Doc Strings
# ///////////////////


FILEPATH_PARAM = ":param filepath: The input file for this function."


# ///////////////////
# FUNCTIONS
# ///////////////////


def get_ffprobe_data(filepath: str) -> Dict[str, Any]:
    f"""
    Get data about a video file via ffprobe.
    {FILEPATH_PARAM}
    :return dict
    """
    return FFProbe(filepath).all()


def get_first_frame(filepath: str, out_filepath: Optional[str] = None) -> str:
    f"""
    Get the first frame of a video. You can use this function to generate a default thumbnail for a video.
    {FILEPATH_PARAM}
    :param out_filepath: an optional output filepath (with 'png' or 'jpeg' extension)
    :return str
    """
    if not out_filepath:
        out_filepath = path.get_tempfile(ext="png")
    command = f"ffmpeg -i '{filename}' -ss 00:00:00.000 -vframes 1 '{out_filepath}'"
    process = path.exec(command)
    if not process.ok:
        raise Exception(
            f"command `{command}` failed because of:{'*'*60}`{process.stderr or process.stdout}`{'*'*60}"
        )
    if not path.exists(out_filepath):
        raise Exception(
            f" command `{command}` did not successfully create file: '{out_filepath}'"
        )
    return out_filepath
