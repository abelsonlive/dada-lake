from dada.types import T
from dada.models.core import db
from dada.models.base import DBTable
from dada.models.field import FieldCacheMixin
from dada.models.mixin import GroupTableMixin, UserMixin
from dada.models.tag_join_table import JobTag
from dada.models.theme_table import JobTheme
from dada.config import settings


class Job(DBTable, GroupTableMixin, FieldCacheMixin, UserMixin):
    """
    A job runs tasks or macros on a schedule
    """

    __tablename__ = "job"
    __module__ = "dada.models.job"
    __defaults__ = settings.JOB_DEFAULTS
    __id_fields__ = ["id", "name", "slug"]
    __tag_join_table__ = JobTag
    __theme_table__ = JobTheme

    status = db.Column(T.text.col, index=True)
    error_message = db.Column(T.text.col, index=False)
    schedule = db.Column(T.cron.col, index=False)

    tags = db.relationship("Tag", secondary="job_tag", lazy="joined")
    theme = db.relationship("JobTheme", lazy="joined")

    __table_args__ = (
        db.Index(f"job_name_uniq_idx", "name", unique=True),
        db.Index(f"job_slug_uniq_idx", "slug", unique=True),
        db.Index("job_vector_idx", "vector", postgresql_using="gin"),
    )
