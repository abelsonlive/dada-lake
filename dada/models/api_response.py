"""
Marshmallow Response Objects Via Flask-Marshmallow 

Seet: https://marshmallow.readthedocs.io/en/stable/api_reference.html

TOOD: Can we just do this inside of resources.core now?

"""
from typing import List

from flask import Response
from marshmallow import fields

from dada.types import T, NewVal
from dada.models.core import db, ma

from dada.models.source import Source

from dada.models.user import User
from dada.models.tag import Tag
from dada.models.file import File
from dada.models.edge import Edge
from dada.models.field import Field, FieldTheme
from dada.models.graph import Graph
from dada.models.macro import Macro
from dada.models.hook import Hook
from dada.models.task import Task
from dada.models.job import Job
from dada.models.folder import Folder
from dada.models.desktop import Desktop

from dada.models.user_source import UserSource
from dada.models.source_file import SourceFile
from dada.models.file_folder import FileFolder
from dada.models.file_desktop import FileDesktop
from dada.models.folder_desktop import FolderDesktop
from dada.models.macro_hook import MacroHook
from dada.models.macro_task import MacroTask
from dada.models.macro_job import MacroJob
from dada.models.task_job import TaskJob
from dada.models.task_hook import TaskHook

from dada.models.tag_join_table import *
from dada.models.theme_table import *

from dada.utils.serde import jsonify


# ///////////////////
# Base Response
# ///////////////////
class BaseResponse(ma.SQLAlchemyAutoSchema):
    class Meta:
        sqla_session = db.session
        strict = True


## /////////////////////////////////
# Source
## /////////////////////////////////


class UserSourceResponse(BaseResponse):
    __gldb_type__ = "user_source"

    class Meta:
        model = UserSource


class SourceFileResponse(BaseResponse):
    __gldb_type__ = "source_file"

    class Meta:
        model = SourceFile


## /////////////////////////////////
### File
## /////////////////////////////////


class FileFolderResponse(BaseResponse):
    __gldb_type__ = "file_folder"

    class Meta:
        model = FileFolder


class FileDesktopResponse(BaseResponse):
    __gldb_type__ = "file_desktop"

    class Meta:
        model = FileDesktop


class FolderDesktopResponse(BaseResponse):
    __gldb_type__ = "folder_desktop"

    class Meta:
        model = FolderDesktop


## /////////////////////////////////
### Pipeline
## /////////////////////////////////


class MacroHookResponse(BaseResponse):
    __gldb_type__ = "macro_hook"

    class Meta:
        model = MacroHook


class MacroTaskResponse(BaseResponse):
    __gldb_type__ = "macro_task"

    class Meta:
        model = MacroTask


class MacroJobResponse(BaseResponse):
    __gldb_type__ = "macro_task"

    class Meta:
        model = MacroJob


class TaskJobResponse(BaseResponse):
    __gldb_type__ = "task_job"

    class Meta:
        model = TaskJob


class TaskHookResponse(BaseResponse):
    __gldb_type__ = "task_hook"

    class Meta:
        model = TaskHook


# ///////////////////////
# Theme Table Responses
# //////////////////////


class EdgeThemeResponse(BaseResponse):
    __gldb_type__ = "edge_theme"

    class Meta:
        model = EdgeTheme


class FileThemeResponse(BaseResponse):
    __gldb_type__ = "file_theme"

    class Meta:
        model = FileTheme


class FolderThemeResponse(BaseResponse):
    __gldb_type__ = "folder_theme"

    class Meta:
        model = FolderTheme


class DesktopThemeResponse(BaseResponse):
    __gldb_type__ = "desktop_theme"

    class Meta:
        model = DesktopTheme


class SourceThemeResponse(BaseResponse):
    __gldb_type__ = "source_theme"

    class Meta:
        model = SourceTheme


class HookThemeResponse(BaseResponse):
    __gldb_type__ = "hook_theme"

    class Meta:
        model = HookTheme


class MacroThemeResponse(BaseResponse):
    __gldb_type__ = "macro_theme"

    class Meta:
        model = MacroTheme


class TaskThemeResponse(BaseResponse):
    __gldb_type__ = "task_theme"

    class Meta:
        model = TaskTheme


class JobThemeResponse(BaseResponse):
    __gldb_type__ = "job_theme"

    class Meta:
        model = JobTheme


class TagThemeResponse(BaseResponse):
    __gldb_type__ = "tag_theme"

    class Meta:
        model = TagTheme


class FieldThemeResponse(BaseResponse):
    __gldb_type__ = "field_theme"

    class Meta:
        model = TagTheme


class UserThemeResponse(BaseResponse):
    __gldb_type__ = "user_theme"

    class Meta:
        model = TagTheme


# ////////////////////////
# Tag Join Table Responses
# ///////////////////////


class EdgeTagResponse(BaseResponse):
    __gldb_type__ = "edge_tag"

    class Meta:
        model = EdgeTag


class FileTagResponse(BaseResponse):
    __gldb_type__ = "file_tag"

    class Meta:
        model = FileTag


class FolderTagResponse(BaseResponse):
    __gldb_type__ = "folder_tag"

    class Meta:
        model = FolderTag


class DesktopTagResponse(BaseResponse):
    __gldb_type__ = "desktop_tag"

    class Meta:
        model = DesktopTag


class SourceTagResponse(BaseResponse):
    __gldb_type__ = "source_tag"

    class Meta:
        model = SourceTag


class HookTagResponse(BaseResponse):
    __gldb_type__ = "hook_tag"

    class Meta:
        model = HookTag


class MacroTagResponse(BaseResponse):
    __gldb_type__ = "macro_tag"

    class Meta:
        model = HookTag


class TaskTagResponse(BaseResponse):
    __gldb_type__ = "task_tag"

    class Meta:
        model = TaskTag


class JobTagResponse(BaseResponse):
    __gldb_type__ = "job_tag"

    class Meta:
        model = JobTag


class TagTagResponse(BaseResponse):
    __gldb_type__ = "tag_tag"

    class Meta:
        model = TagTag


class FieldTagResponse(BaseResponse):
    __gldb_type__ = "field_tag"

    class Meta:
        model = FieldTag


class UserTagResponse(BaseResponse):
    __gldb_type__ = "user_tag"

    class Meta:
        model = UserTag


# ///////////////////
# low-level models
# ////////////////////


class TagResponse(BaseResponse):
    __gldb_type__ = "tag"

    theme = NewVal("tag_theme", fields.Nested(TagThemeResponse))

    class Meta:
        model = Tag
        exclude = ["vector"]


class FieldResponse(BaseResponse):
    __gldb_type__ = "field"

    core = T.bool.val

    tags = NewVal("field_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("field_theme", fields.Nested(FieldThemeResponse))

    class Meta:
        model = Field
        exclude = ["vector"]


# ////////////////////////////
# Core Models
# ////////////////////////////


class EdgeResponse(BaseResponse):
    __gldb_type__ = "edge"

    # relationships
    tags = NewVal("edge_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("edge_theme", fields.Nested(EdgeThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Edge
        exclude = ["vector"]


class GraphResponse(BaseResponse):
    __gldb_type__ = "graph"

    class Meta(BaseResponse.Meta):
        model = Graph


class DesktopResponse(BaseResponse):
    __gldb_type__ = "desktop"

    # relationships
    folders = NewVal(
        "desktop_folder_array", fields.List(fields.Nested(FolderDesktopResponse))
    )
    tags = NewVal("desktop_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("desktop_theme", fields.Nested(DesktopThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Desktop
        exclude = ["vector"]


class FolderResponse(BaseResponse):
    __gldb_type__ = "folder"

    # relationships
    files = NewVal("folder_file_array", fields.List(fields.Nested(FileFolderResponse)))
    desktops = NewVal(
        "folder_desktop_array", fields.List(fields.Nested(DesktopResponse))
    )
    tags = NewVal("folder_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("folder_theme", fields.Nested(FolderThemeResponse))

    class Meta(BaseResponse.Meta):
        exclude = ["vector"]
        model = Folder


class MacroResponse(BaseResponse):
    __gldb_type__ = "macro"

    tags = NewVal("macro_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("macro_theme", fields.Nested(MacroThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Macro
        exclude = ["vector"]


class SourceResponse(BaseResponse):
    __gldb_type__ = "source"

    # relationships
    files = NewVal("source_file_array", fields.List(fields.Nested(SourceFileResponse)))

    tags = NewVal("source_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("source_theme", fields.Nested(SourceThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Source
        exclude = ["vector"]


class TaskResponse(BaseResponse):
    __gldb_type__ = "task"
    theme = NewVal("task_theme", fields.Nested(TaskThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Task
        exclude = ["vector"]


class HookResponse(BaseResponse):
    __gldb_type__ = "hook"

    tasks = NewVal("hook_task_array", fields.List(fields.Nested(TaskResponse)))
    theme = NewVal("hook_theme", fields.Nested(HookThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Hook


class JobResponse(BaseResponse):
    __gldb_type__ = "job"

    tasks = NewVal("job_task_array", fields.List(fields.Nested(TaskJobResponse)))
    theme = NewVal("job_theme", fields.Nested(JobThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Job
        exclude = ["vector"]


class FileResponse(BaseResponse):
    __gldb_type__ = "file"
    theme = NewVal("file_theme", fields.Nested(FileThemeResponse))

    # urls
    s3_urls = fields.Dict()

    human_size = T.text.val

    folders = NewVal("folder_array", fields.List(fields.Nested(FolderResponse)))
    tags = NewVal("folder_tag_array", fields.List(fields.Nested(TagResponse)))

    # macro/task/job
    macro = fields.Nested(MacroResponse, only=("id", "slug"))
    task = fields.Nested(TaskResponse, only=("id", "slug"))
    job = fields.Nested(JobResponse, only=("id", "slug"))

    class Meta(BaseResponse.Meta):
        model = File
        exclude = ["vector"]


class UserResponse(BaseResponse):

    __gldb_type__ = "user"

    tags = NewVal("user_tag_array", fields.List(fields.Nested(TagResponse)))
    files = NewVal(
        "user_file_array",
        fields.List(
            fields.Nested(
                FileResponse,
                only=("id", "slug"),
            )
        ),
    )
    folders = NewVal(
        "user_folder_array",
        fields.List(fields.Nested(FolderResponse, only=("id", "slug"))),
    )
    desktops = NewVal(
        "user_desktop_array",
        fields.List(fields.Nested(DesktopResponse, only=("id", "slug"))),
    )
    theme = NewVal("user_theme", fields.Nested(UserThemeResponse))

    class Meta(BaseResponse.Meta):
        model = User
        exclude = ["vector"]
