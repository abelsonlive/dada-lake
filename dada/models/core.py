"""
Core Objects and Connections for Access in Sickdb Models
"""
from flask import Response
from flask_marshmallow import Marshmallow

import hiredis  # force this dependency for performance
import redis
from sqlalchemy import create_engine, MetaData
from flask_sqlalchemy import SQLAlchemy
from dada.exc import RequestError

from dada.types import T
from dada.utils import path, s3
from dada.config import settings

# /////////
# API Database Connection
# /////////
db = SQLAlchemy()

# s3 connection
s3conn = s3.Bucket(
    bucket_name=settings.S3_BUCKET,
    aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
    aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
)

# ma<-> sql alchemy schema engine
ma = Marshmallow()

# redis cache connection
rdsconn = redis.from_url(
    settings.REDIS_CACHE_URL, db=settings.REDIS_CACHE_DB, **settings.REDIS_CACHE_KWARGS
)

# For running other queries not in the app context.
DB_ENGINE = create_engine(settings.SQLALCHEMY_DATABASE_URI)
DB_META = MetaData(bind=DB_ENGINE, reflect=True)


def db_execute(query: T.sql.py):
    """
    Execute a query against the database outside of the appplication context.
    :yield dict
    """
    with DB_ENGINE.connect() as connection:
        result = connection.execute(query)
        if result.returns_rows:
            return (dict(row.items()) for row in result)
        else:
            return True
