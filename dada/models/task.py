"""
A task runs a series of Macros with preset paramaters.

You can also pass in parameters at runtime to modify how the task behaves 

Tasks can be 'chained' which means that the first macro passes the output to the second macro, etc. 
- This only works if each macro accepts and returns the same types. 
- An error will be thrown if you attempt to chain macros that don't have the same type signatures
- You can, however, chain FILE macros with different `file_type`s and `file_subtypes`.
  in this case, any file passed to a macro which doesn't accept that file;s `type` or `subtype` will 
  simply be passed through to the next macro. this allows up to chain up many functions to handle complext processing tasks 
  for isntance, the 'extract fields' task chains macros which apply generally to all files as well as macros which apply specifically to a given `type` or `subtype` 
  this allows us to have a singular interface for processing any file while retaining the flexibility of running an individual macro as a one off. 
  we can also modify a task simply by adding/removing a macro or chanding the order in which the macros run. 


Tasks that are not chained simply trigger multiple macros in parallel 
- eg "check for new music" task would trigger the 'get music from youtube', the 'get music from soundcloud', the 'get music from slsk' macros 
- since these macros are independent of eachother, they can run in parallel 

Tasks can be synced to "hooks", enabling them to run when certain events occur

Tasks can be synced to "jobs", enabling them to be run on a schedule

"""
from dada.types import T
from dada.models.core import db
from dada.models.base import DBTable
from dada.models.mixin import GroupTableMixin, UserMixin
from dada.models.tag_join_table import TaskTag
from dada.models.theme_table import TaskTheme
from dada.models.field import FieldCacheMixin


class Task(DBTable, GroupTableMixin, FieldCacheMixin, UserMixin):
    """"""

    __tablename__ = "task"
    __module__ = "dada.models.task"
    __tag_join_table__ = TaskTag
    __theme_table__ = TaskTheme

    # TODO: Add visual columns here.

    tags = db.relationship("Tag", secondary="task_tag", lazy="joined")
    theme = db.relationship("TaskTheme", lazy="joined")

    __table_args__ = (
        db.Index(f"task_name_uniq_idx", "name", unique=True),
        db.Index("task_vector_idx", "vector", postgresql_using="gin"),
    )
