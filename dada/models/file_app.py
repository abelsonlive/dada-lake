import datetime

from sqlalchemy import ForeignKey, Index

from dada.types import T
from dada.models.base import DBTable
from dada.models.core import db
from dada.models.field import FieldCacheMixin
from dada.models.mixin import JoinFieldTableMixin, PipelineMixin


class FileApp(DBTable, JoinFieldTableMixin, FieldCacheMixin, PipelineMixin):
    """"""

    __tablename__ = "file_app"
    __module__ = "dada.models.file_app"
    __from_id__ = "file_id"
    __to_id__ = "app_id"

    file_id = db.Column(T.file_id.col, ForeignKey("file.id"), index=True)
    app_id = db.Column(T.app_id.col, ForeignKey("app.id"), index=True)

    # an app can only have one file
    __table_args__ = (Index("file_app_uniq_idx", "app_id", "file_id", unique=True),)

    # relationships
    file = db.relationship("File", lazy=True)
    app = db.relationship("App", lazy=True)
