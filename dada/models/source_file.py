import datetime

from sqlalchemy import ForeignKey, func, Index

from dada.types import T
from dada.models.base import DBTable
from dada.models.core import db
from dada.models.mixin import JoinFieldTableMixin
from dada.models.field import FieldCacheMixin


class SourceFile(DBTable, JoinFieldTableMixin, FieldCacheMixin):

    __tablename__ = "source_file"
    __module__ = "dada.models.source_file"
    __from_id__ = "source_id"
    __to_id__ = "file_id"

    source_id = db.Column(T.source_id.col, ForeignKey("source.id"), index=True)
    file_id = db.Column(T.file_id.col, ForeignKey("file.id"), index=True)

    __table_args__ = (
        Index("source_file_uniq_idx", "source_id", "file_id", unique=True),
    )

    # relationships
    source = db.relationship("Source", lazy=True)
    file = db.relationship("File", lazy=True)
