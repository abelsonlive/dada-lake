"""
"""
import logging


from sqlalchemy import func
from sqlalchemy.dialects.postgresql import ENUM


from dada.types import T
from dada.models.core import db
from dada.models.base import DBTable
from dada.models.mixin import SlugTableMixin
from dada.config import settings
from dada.models.tag_join_table import HookTag
from dada.models.theme_table import HookTheme

TAG_MODEL_LOGGER = logging.getLogger()


class Hook(DBTable, SlugTableMixin):
    """
    A hook triggers tasks or job when an event happens (eq `file` `created`)
    Data from that event is passed as input to the hook.
    """

    __tablename__ = "hook"
    __module__ = "dada.models.hook"
    __id_fields__ = ["id", "name", "slug"]
    __defaults__ = settings.HOOK_DEFAULTS
    __tag_join_table__ = HookTag
    __theme_table__ = HookTheme

    # tags limited by entity types
    entity_type = db.Column(
        T.entity_type.col(
            *(settings.HOOK_DEFAULTS_ENTITY_TYPES),
            name="hook_entity_types_enum",
            create_type=True,
        ),
        index=True,
    )
    action = db.Column(
        T.enum.col(
            *(settings.HOOK_DEFAULTS_ACTIONS),
            name="hook_action_enum",
            craeate_type=True,
        ),
        index=True,
    )

    tags = db.relationship("Tag", secondary="hook_tag", lazy=True)
    theme = db.relationship("HookTheme", lazy=True)

    # Tasks associated with this hook.
    macros = db.relationship("Macro", secondary="macro_hook", lazy=True)

    # Tasks associated with this hook.
    tasks = db.relationship("Task", secondary="task_hook", lazy=True)

    __table_args__ = (
        db.Index(f"hook_name_uniq_idx", "name", unique=True),
        db.Index(f"hook_slug_uniq_idx", "slug", unique=True),
    )
