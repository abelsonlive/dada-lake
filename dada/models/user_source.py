from sqlalchemy import ForeignKey, Index

from dada.types import T
from dada.models.base import DBTable
from dada.models.core import db
from dada.models.field import FieldCacheMixin
from dada.models.mixin import JoinFieldTableMixin, UserMixin


class UserSource(DBTable, JoinFieldTableMixin, FieldCacheMixin, UserMixin):

    __tablename__ = "user_source"
    __module__ = "dada.models.user_source"
    __from_id__ = "user_id"
    __to_id__ = "source_id"

    source_id = db.Column(T.source_id.col, ForeignKey("source.id"), index=True)

    # relationships
    source = db.relationship("Source", lazy=True)
