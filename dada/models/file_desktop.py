import datetime

from sqlalchemy import ForeignKey, func, Index

from dada.models.base import DBTable
from dada.models.core import db
from dada.models.field import FieldCacheMixin
from dada.models.mixin import JoinFieldTableMixin, PipelineMixin, PositionalTableMixin


class FileDesktop(
    DBTable, JoinFieldTableMixin, PositionalTableMixin, PipelineMixin, FieldCacheMixin
):

    __tablename__ = "file_desktop"
    __module__ = "dada.models.file_desktop"
    __from_id__ = "file_id"
    __to_id__ = "desktop_id"

    file_id = db.Column(db.Integer, ForeignKey("file.id"), index=True)
    desktop_id = db.Column(db.Integer, ForeignKey("desktop.id"), index=True)

    __table_args__ = (
        Index(
            "file_desktop_uniq_idx", "file_id", "desktop_id", "position", unique=True
        ),
    )

    # relationships

    file = db.relationship("File", lazy=True)
    desktop = db.relationship("Desktop", lazy=True)
