"""
A source represents metadata about an external file provider, including othing sickdb hosts
"""
import logging

from sqlalchemy import func
from sqlalchemy.dialects.postgresql import ENUM


from dada.types import T
from dada.models.core import db
from dada.models.base import DBTable
from dada.models.mixin import GroupTableMixin, PipelineMixin
from dada.config import settings
from dada.models.tag_join_table import SourceTag
from dada.models.theme_table import SourceTheme
from dada.models.field import FieldCacheMixin

TAG_MODEL_LOGGER = logging.getLogger()


class Source(DBTable, GroupTableMixin, PipelineMixin, FieldCacheMixin):

    __tablename__ = "source"
    __module__ = "dada.models.source"
    __id_fields__ = ["id", "name", "slug"]
    __defaults__ = settings.SOURCE_DEFAULTS
    __tag_join_table__ = SourceTag
    __theme_table__ = SourceTheme

    # sources limited by entity types
    # eg bandcamp transmits audio, video, image, but without api it does not receive these entity type
    # eq youtube transmits audio, video, image, and text and also receives it because of its api
    transmits_entity_types = db.Column(
        T.entity_type_array.col(
            T.entity_type.col(
                *settings.SOURCE_DEFAULTS_TRANSMITS_ENTITY_TYPES,
                name="source_transmits_entity_types_enum",
                create_type=True,
            )
        ),
        index=True,
        default=None,
    )

    transmits_file_types = db.Column(
        T.file_type_array.col(
            T.file_type.col(
                *T.file_type.opts,
                name="source_transmits_file_types_enum",
                create_type=True,
            )
        ),
        index=True,
        default=None,
    )

    transmits_file_subtypes = db.Column(
        T.file_subtype_array.col(
            T.file_subtype.col(
                *T.file_subtype.opts,
                name="source_transmits_file_subtypes_enum",
                create_type=True,
            )
        ),
        index=True,
        default=None,
    )

    # sources limited by entity types they receive
    accepts_entity_types = db.Column(
        T.entity_type_array.col(
            T.entity_type.col(
                *settings.SOURCE_DEFAULTS_ACCEPTS_ENTITY_TYPES,
                name="source_accepts_entity_types_enum",
                create_type=True,
            )
        ),
        index=True,
        default=None,
    )

    accepts_file_types = db.Column(
        T.file_type_array.col(
            T.file_type.col(
                *T.file_type.opts,
                name="source_accepts_file_subtypes_enum",
                create_type=True,
            )
        ),
        index=True,
        default=None,
    )

    accepts_file_subtypes = db.Column(
        T.file_subtype_array.col(
            T.file_subtype.col(
                *T.file_subtype.opts,
                name="source_accepts_file_subtypes_enum",
                create_type=True,
            )
        ),
        index=True,
        default=None,
    )

    # tag theme
    theme = db.relationship("SourceTheme", lazy="joined")

    # files
    files = db.relationship("SourceFile", lazy=True)

    __table_args__ = (
        db.Index(f"source_slug_uniq_idx", "slug", unique=True),
        db.Index("source_vector_idx", "vector", postgresql_using="gin"),
    )

    def to_dict(self) -> dict:
        """
        Serializable object
        """
        d = {
            "id": self.id,
            "user_id": self.user_id,
            "name": self.name,
            "slug": self.slug,
            "emoji": self.emoji_char,
            "info": self.info,
            "accepts_entity_types": self.accepts_entity_types,
            "transmits_entity_types": self.transmits_file_types,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "fields": self.fields,
        }
        return d
