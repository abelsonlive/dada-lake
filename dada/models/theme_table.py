import logging

from dada.types import T
from dada.models.base import DBTable
from dada.models.core import db
from dada.models.field import FieldCacheMixin
from dada.models.mixin import ThemeTableMixin, PipelineMixin
from dada.config import settings

TAG_JOIN_MODEL_LOGGER = logging.getLogger()


class EdgeTheme(DBTable, ThemeTableMixin, PipelineMixin):

    __tablename__ = "edge_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["edge_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    edge_id = db.Column(T.edge_id.col, db.ForeignKey("edge.id"), index=True)

    # relationships
    edge = db.relationship("Edge", lazy=True)

    __table_args__ = (db.Index(f"edge_theme_uniq_edge_idx", "edge_id", unique=True),)


class FileTheme(DBTable, ThemeTableMixin, PipelineMixin):

    __tablename__ = "file_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["file_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    file_id = db.Column(T.file_id.col, db.ForeignKey("file.id"), index=True)

    # relationships
    file = db.relationship("File", lazy=True)

    __table_args__ = (db.Index(f"file_theme_uniq_file_idx", "file_id", unique=True),)


class FolderTheme(DBTable, ThemeTableMixin, PipelineMixin):

    __tablename__ = "folder_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["folder_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    folder_id = db.Column(T.folder_id.col, db.ForeignKey("folder.id"), index=True)

    # relationships
    folder = db.relationship("Folder", lazy=True)

    __table_args__ = (
        db.Index(f"folder_theme_uniq_folder_idx", "folder_id", unique=True),
    )


class DesktopTheme(DBTable, ThemeTableMixin, PipelineMixin):

    __tablename__ = "desktop_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["desktop_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    desktop_id = db.Column(T.desktop_id.col, db.ForeignKey("desktop.id"), index=True)

    # relationships
    desktop = db.relationship("Desktop", lazy=True)

    __table_args__ = (
        db.Index(f"desktop_theme_uniq_desktop_idx", "desktop_id", unique=True),
    )


class UserTheme(DBTable, ThemeTableMixin, PipelineMixin):

    __tablename__ = "user_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["user_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    user_id = db.Column(T.user_id.col, db.ForeignKey("user.id"), index=True)

    # relationships
    user = db.relationship("User", lazy=True)

    __table_args__ = (db.Index(f"user_theme_uniq_user_idx", "user_id", unique=True),)


class HookTheme(DBTable, ThemeTableMixin, PipelineMixin):

    __tablename__ = "hook_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["hook_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    hook_id = db.Column(T.hook_id.col, db.ForeignKey("hook.id"), index=True)

    # relationships
    hook = db.relationship("Hook", lazy=True)

    __table_args__ = (db.Index(f"hook_theme_uniq_hook_idx", "hook_id", unique=True),)


class TagTheme(DBTable, ThemeTableMixin, PipelineMixin):

    __tablename__ = "tag_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["tag_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    tag_id = db.Column(T.tag_id.col, db.ForeignKey("tag.id"), index=True)

    # relationships
    tag = db.relationship("Tag", lazy=True)

    __table_args__ = (db.Index(f"tag_theme_uniq_tag_idx", "tag_id", unique=True),)


class SourceTheme(DBTable, ThemeTableMixin, PipelineMixin):

    __tablename__ = "source_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["source_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    source_id = db.Column(T.source_id.col, db.ForeignKey("source.id"), index=True)

    # relationships
    source = db.relationship("Source", lazy=True)

    __table_args__ = (
        db.Index(f"source_theme_uniq_source_idx", "source_id", unique=True),
    )


#
# Pipline themes can only be manually created (not by pipline tasks)
#


class MacroTheme(DBTable, ThemeTableMixin):

    __tablename__ = "macro_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["macro_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    macro_id = db.Column(T.macro_id.col, db.ForeignKey("macro.id"), index=True)

    # relationships
    macro = db.relationship("Macro", lazy=True)

    __table_args__ = (db.Index(f"macro_theme_uniq_macro_idx", "macro_id", unique=True),)


class TaskTheme(DBTable, ThemeTableMixin):

    __tablename__ = "task_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["task_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    task_id = db.Column(T.task_id.col, db.ForeignKey("task.id"), index=True)

    # relationships
    task = db.relationship("Task", lazy=True)
    __table_args__ = (db.Index(f"task_theme_uniq_task_idx", "task_id", unique=True),)


class JobTheme(DBTable, ThemeTableMixin):

    __tablename__ = "job_theme"
    __module__ = "dada.models.theme_table"
    __id_columns__ = ["job_id"]
    __internal_columns__ = [
        "created_at",
        "updated_at",
        "id",
    ]
    job_id = db.Column(T.job_id.col, db.ForeignKey("job.id"), index=True)

    # relationships
    job = db.relationship("Job", lazy=True)
    __table_args__ = (db.Index(f"job_theme_uniq_job_idx", "job_id", unique=True),)
