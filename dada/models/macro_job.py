import datetime

from sqlalchemy import ForeignKey, func, Index

from dada.types import T
from dada.models.base import DBTable
from dada.models.core import db
from dada.models.field import FieldCacheMixin
from dada.models.mixin import JoinFieldTableMixin, PositionalTableMixin


class MacroJob(DBTable, JoinFieldTableMixin, FieldCacheMixin, PositionalTableMixin):

    __tablename__ = "macro_job"
    __module__ = "dada.models.macro_job"
    __from_id__ = "macro_id"
    __to_id__ = "job_id"

    macro_id = db.Column(T.macro_id.col, ForeignKey("macro.id"), index=True)
    job_id = db.Column(T.job_id.col, ForeignKey("job.id"), index=True)

    __table_args__ = (
        Index("macro_job_uniq_idx", "macro_id", "job_id", "position", unique=True),
    )

    # relationships
    macro = db.relationship("Macro", lazy=True)
    job = db.relationship("Job", lazy=True)
