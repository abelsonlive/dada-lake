from sqlalchemy import ForeignKey, func, Index

from dada.types import T
from dada.models.core import db
from dada.models.base import DBTable
from dada.models.mixin import JoinFieldTableMixin
from dada.models.field import FieldCacheMixin


class TaskHook(DBTable, JoinFieldTableMixin, FieldCacheMixin):

    __tablename__ = "task_hook"
    __module__ = "dada.models.task_hook"
    __from_id__ = "task_id"
    __to_id__ = "hook_id"

    task_id = db.Column(T.task_id.col, ForeignKey("task.id"), index=True)
    hook_id = db.Column(T.hook_id.col, ForeignKey("hook.id"), index=True)

    __table_args__ = (Index("task_hook_uniq_idx", "task_id", "hook_id", unique=True),)

    # relationships
    task = db.relationship("Task", lazy=True)
    hook = db.relationship("Hook", lazy=True)
