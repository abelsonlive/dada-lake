"""
A polymorphic grah of node id -> edge_id -> node_id (attibutes)
"""
from dada.types import T
from dada.models.core import db
from dada.models.base import DBTable
from dada.models.mixin import PipelineMixin


class Graph(DBTable, PipelineMixin):
    """
    TODO: define polymorphic relationship via sqlalchemy
    --> https://docs.sqlalchemy.org/en/13/orm/inheritance_loading.html#using-with-polymorphic
    """

    __tablename__ = "graph"
    __module__ = "dada.models.graph"

    from_id = db.Column(T.fk.col, index=True)
    to_id = db.Column(T.fk.col, index=True)
    edge_id = db.Column(T.edge_id.col, db.ForeignKey("edge.id"), index=True)
    attr = db.Column(T.json.col, default={})

    # relationships
    edge = db.relationship("Edge", lazy="joined")

    __table_args__ = (
        db.Index("graph_unique_idx", "from_id", "to_id", "edge_id", unique=True),
    )

    @property
    def edge_name(self):
        return self.edge.name

    @property
    def edge_backref(self):
        return self.edge.backref or self.edge.name

    @property
    def edge_slug(self):
        return self.edge.slug

    @property
    def edge_type(self):
        return self.edge.edge_type

    @property
    def attr_type(self):
        return self.edge.attr_type

    @property
    def from_name(self):
        return self.edge_name

    @property
    def to_name(self):
        return self.edge_backref
