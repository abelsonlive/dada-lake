"""
Tag Join Tables
TODO: figure out how to dry this up?
"""
import logging

from dada.types import T
from dada.models.base import DBTable
from dada.models.core import db
from dada.models.mixin import (
    TagTableMixin,
    JoinTableMixin,
    JoinTableMixin,
    PipelineMixin,
)
from dada.config import settings

TAG_JOIN_MODEL_LOGGER = logging.getLogger()


class EdgeTag(DBTable, TagTableMixin, JoinTableMixin, PipelineMixin):

    __tablename__ = "edge_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "edge_id"

    edge_id = db.Column(T.edge_id.col, db.ForeignKey("edge.id"), index=True)

    # relationships
    edge = db.relationship("Edge", lazy=True)


class FileTag(DBTable, TagTableMixin, JoinTableMixin, PipelineMixin):

    __tablename__ = "file_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "file_id"

    file_id = db.Column(T.file_id.col, db.ForeignKey("file.id"), index=True)

    # relationships
    file = db.relationship("File", lazy=True)


class FieldTag(DBTable, TagTableMixin, JoinTableMixin, PipelineMixin):

    __tablename__ = "field_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "field_id"

    field_id = db.Column(T.field_id.col, db.ForeignKey("field.id"), index=True)
    # relationships
    field = db.relationship("Field", lazy=True)


class FolderTag(DBTable, TagTableMixin, JoinTableMixin, PipelineMixin):

    __tablename__ = "folder_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "folder_id"

    folder_id = db.Column(T.folder_id.col, db.ForeignKey("folder.id"), index=True)

    # relationships
    folder = db.relationship("Folder", lazy=True)


class DesktopTag(DBTable, TagTableMixin, JoinTableMixin, PipelineMixin):

    __tablename__ = "desktop_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "desktop_id"

    desktop_id = db.Column(T.desktop_id.col, db.ForeignKey("desktop.id"), index=True)

    # relationships
    desktop = db.relationship("Desktop", lazy=True)


class UserTag(DBTable, TagTableMixin, JoinTableMixin, PipelineMixin):

    __tablename__ = "user_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "user_id"

    user_id = db.Column(T.user_id.col, db.ForeignKey("user.id"), index=True)

    # relationships
    user = db.relationship("User", lazy=True)


class SourceTag(DBTable, TagTableMixin, JoinTableMixin, PipelineMixin):

    __tablename__ = "source_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "source_id"

    source_id = db.Column(T.source_id.col, db.ForeignKey("source.id"), index=True)
    # relationships
    source = db.relationship("Source", lazy=True)


# //////////////////////////////////////////////////////////////////
# Pipeline models can only be manually tagged (not tagged by macros)
# //////////////////////////////////////////////////////////////////


class HookTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "hook_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "hook_id"

    hook_id = db.Column(T.hook_id.col, db.ForeignKey("hook.id"), index=True)
    # relationships
    hook = db.relationship("Hook", lazy=True)


class MacroTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "macro_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "macro_id"

    macro_id = db.Column(T.macro_id.col, db.ForeignKey("macro.id"), index=True)
    # relationships
    function = db.relationship("Macro", lazy=True)


class TaskTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "task_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "task_id"

    task_id = db.Column(T.task_id.col, db.ForeignKey("task.id"), index=True)
    # relationships
    task = db.relationship("Task", lazy=True)


class JobTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "job_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "job_id"

    job_id = db.Column(T.job_id.col, db.ForeignKey("job.id"), index=True)
    # relationships
    job = db.relationship("Job", lazy=True)


# //////////////////////////////////////////////////////////////////
# Etc.
# //////////////////////////////////////////////////////////////////


class TagTag(DBTable, JoinTableMixin, PipelineMixin):
    """
    A mapping of tagged tags?
    """

    __tablename__ = "tag_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "from_tag_id"
    __to_id__ = "to_tag_id"

    from_tag_id = db.Column(T.tag_id.col, db.ForeignKey("tag.id"), index=True)
    to_tag_id = db.Column(T.tag_id.col, db.ForeignKey("tag.id"), index=True)

    # relationships
    from_tag = db.relationship("Tag", foreign_keys=[from_tag_id], lazy=True)
    to_tag = db.relationship("Tag", foreign_keys=[to_tag_id], lazy=True)
