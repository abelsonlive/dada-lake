import logging

from sqlalchemy.dialects.postgresql import ENUM

from dada.types import T
from dada.models.core import db
from dada.models.field import FieldCacheMixin
from dada.models.base import DBTable
from dada.models.mixin import (
    SlugTableMixin,
    GLDBTypeTableMixin,
    PipelineMixin,
    SearchTableMixin,
)
from dada.models.tag_join_table import EdgeTag
from dada.models.theme_table import EdgeTheme
from dada.types.dictionary import SIMPLE_TYPE_NAMES
from dada.config import settings

EDGE_LOGGER = logging.getLogger()


class Edge(
    DBTable, SlugTableMixin, SearchTableMixin, GLDBTypeTableMixin, PipelineMixin
):
    # TODO: Figure out how to define sql alchemy relations for graph ish
    __tablename__ = "edge"
    __module__ = "dada.models.edge"
    __id_fields__ = ["id", "name", "slug", "backref"]
    __type_column_name__ = "attr_type"
    __defaults__ = settings.EDGE_DEFAULTS
    __tag_join_table__ = EdgeTag
    __theme_table__ = EdgeTheme

    backref = db.Column(T.text.col, index=True, default=None)
    edge_type = db.Column(
        T.enum.col(
            *settings.EDGE_DEFAULTS_EDGE_TYPES, name="edge_type_enum", create_type=True
        ),
        index=True,
    )

    from_entity_type = db.Column(
        T.entity_type.col(
            *settings.EDGE_DEFAULTS_ENTITY_TYPES,
            name="edge_from_entity_type_enum",
            create_type=True
        ),
        index=True,
    )
    from_file_type = db.Column(
        T.file_type.col(
            *settings.FILE_DEFAULTS_FILE_TYPES_ALL,
            name="edge_from_file_type_enum",
            create_type=True
        ),
        default=None,
        index=True,
    )
    from_file_subtype = db.Column(
        T.file_subtype.col(
            *settings.FILE_DEFAULTS_FILE_SUBTYPES_ALL,
            name="edge_from_file_subtype_enum",
            create_type=True
        ),
        default=None,
        index=True,
    )

    to_entity_type = db.Column(
        T.entity_type.col(
            *settings.EDGE_DEFAULTS_ENTITY_TYPES,
            name="edge_to_entity_type_enum",
            create_type=True
        ),
        default=None,
        index=True,
    )
    to_file_type = db.Column(
        T.file_type.col(
            *settings.FILE_DEFAULTS_FILE_TYPES_ALL,
            name="edge_to_file_type_enum",
            create_type=True
        ),
        default=None,
        index=True,
    )
    to_file_subtype = db.Column(
        T.file_subtype.col(
            *settings.FILE_DEFAULTS_FILE_SUBTYPES_ALL,
            name="edge_to_file_subtype_enum",
            create_type=True
        ),
        default=None,
        index=True,
    )

    # this determines the type of the graph attribute
    attr_type = db.Column(
        T.type.col(*SIMPLE_TYPE_NAMES, name="edge_attr_type_enum", create_type=True),
        default=None,
        index=True,
    )

    # relationships

    tags = db.relationship("Tag", secondary="edge_tag", lazy="joined")
    theme = db.relationship(
        "EdgeTheme",
        lazy="joined",
    )

    __table_args__ = (
        db.Index(
            "edge_uniq_idx",
            "name",
            "edge_type",
            "from_entity_type",
            "from_file_type",
            "from_file_subtype",
            "to_entity_type",
            "to_file_type",
            "to_file_subtype",
            "attr_type",
            unique=True,
        ),
    )

    @classmethod
    def create(cls, *args, **kwargs):

        # instantiate  object
        instance = cls()
        instance.set_cols(**kwargs)
        # backref should default to name
        if instance.backref is None:
            instance.backref = instance.name
        return instance
