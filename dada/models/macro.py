"""
A func performs an operation on an entity and mutates it, returning 0-N of the same entity 
Think of it like a unix pipe but for our core types

like a tag or a field, a func can apply to multiple entity types / file types
"""
from sqlalchemy.dialects.postgresql import ENUM, JSONB

from dada.types import T
from dada.models.core import db
from dada.models.base import DBTable
from dada.models.field import FieldCacheMixin
from dada.models.mixin import (
    GroupTableMixin,
    FileTypeTableMixin,
)
from dada.models.tag_join_table import MacroTag
from dada.models.theme_table import MacroTheme
from dada.config import settings


class Macro(DBTable, GroupTableMixin, FileTypeTableMixin, FieldCacheMixin):
    """
    Run a function via the api. Functions grab stuff from the web, maniuplate files, and register changes via the API
    """

    __tablename__ = "macro"
    __module__ = "dada.models.macro"
    __defaults__ = settings.MACRO_DEFAULTS
    __tag_join_table__ = MacroTag
    __theme_table__ = MacroTheme

    # func
    entity_type = db.Column(
        T.entity_type.col(
            *(settings.MACRO_DEFAULTS_ENTITY_TYPES),
            name="macro_entity_type_enum",
            create_type=True,
        ),
        index=True,
        default=None,
    )

    # func
    action = db.Column(
        ENUM(
            *(settings.MACRO_DEFAULTS_ACTIONS),
            name="macro_action_enum",
            create_type=True,
        ),
        index=True,
        default=None,
    )

    # funcs limited by entity types
    tags = db.relationship("Tag", secondary="macro_tag", lazy="joined")
    theme = db.relationship("MacroTheme", lazy="joined")

    __table_args__ = (
        db.Index(f"macro_slug_uniq_idx", "slug", unique=True),
        db.Index("macro_vector_idx", "vector", postgresql_using="gin"),
    )

    def to_dict(self):
        """
        TODO!
        """
        return {}
