from sqlalchemy import ForeignKey, func, Index

from dada.types import T
from dada.models.core import db
from dada.models.base import DBTable
from dada.models.field import FieldCacheMixin
from dada.models.mixin import JoinFieldTableMixin, PositionalTableMixin

from dada.utils import dates


class TaskJob(DBTable, JoinFieldTableMixin, FieldCacheMixin, PositionalTableMixin):

    __tablename__ = "task_job"
    __module__ = "dada.models.task_job"
    __from_id__ = "task_id"
    __to_id__ = "job_id"

    task_id = db.Column(T.task_id.col, ForeignKey("task.id"), index=True)
    job_id = db.Column(T.job_id.col, ForeignKey("job.id"), index=True)
    chained = db.Column(T.bool.col, default=False)

    __table_args__ = (
        Index(
            "task_job_position_uniq_idx", "job_id", "task_id", "position", unique=True
        ),
    )

    # relationships
    task = db.relationship("Task", lazy=True)
    job = db.relationship("Job", lazy=True)
