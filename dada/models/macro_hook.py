from sqlalchemy import ForeignKey, func, Index

from dada.types import T
from dada.models.core import db
from dada.models.base import DBTable
from dada.models.mixin import JoinFieldTableMixin
from dada.models.field import FieldCacheMixin


class MacroHook(DBTable, JoinFieldTableMixin, FieldCacheMixin):

    __tablename__ = "macro_hook"
    __module__ = "dada.models.macro_hook"
    __from_id__ = "macro_id"
    __to_id__ = "hook_id"

    macro_id = db.Column(T.macro_id.col, ForeignKey("macro.id"), index=True)
    hook_id = db.Column(T.hook_id.col, ForeignKey("hook.id"), index=True)

    __table_args__ = (Index("macro_hook_uniq_idx", "macro_id", "hook_id", unique=True),)

    # relationships
    macro = db.relationship("Macro", lazy=True)
    hook = db.relationship("Hook", lazy=True)
