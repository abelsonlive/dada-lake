import datetime

from sqlalchemy import ForeignKey, func, Index

from dada.types import T
from dada.models.base import DBTable
from dada.models.core import db
from dada.models.field import FieldCacheMixin
from dada.models.mixin import JoinFieldTableMixin, PositionalTableMixin


class MacroTask(DBTable, JoinFieldTableMixin, FieldCacheMixin, PositionalTableMixin):

    __tablename__ = "macro_task"
    __module__ = "dada.models.macro_task"
    __from_id__ = "macro_id"
    __to_id__ = "task_id"

    macro_id = db.Column(T.macro_id.col, ForeignKey("macro.id"), index=True)
    task_id = db.Column(T.task_id.col, ForeignKey("task.id"), index=True)

    __table_args__ = (
        Index("macro_task_uniq_idx", "macro_id", "task_id", "position", unique=True),
    )

    # relationships
    macro = db.relationship("Macro", lazy=True)
    task = db.relationship("Task", lazy=True)
