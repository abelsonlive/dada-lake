"""
An abstract Redis cache
"""
import logging
import pickle
from hashlib import md5
from typing import Any, Dict

from dada.types import T
from dada.types.base import SerializableObject
from dada.models.core import rdsconn
from dada.utils import dates, serde

CACHE_LOGGER = logging.getLogger()


class CacheResponse(SerializableObject):
    __module__ = "dada.models.cache"
    __gldb_type__ = "cache_response"

    """
    A class that we return from a cache request.
    """

    def __init__(
        self,
        key: T.text.py,
        value: Any,
        last_modified: T.date_tz.py_optional,
        is_cached: T.bool.py,
        debug: T.bool.py,
    ):
        self.key = key
        self.value = value
        self.last_modified = last_modified
        self.is_cached = is_cached
        self.debug = debug

    @property
    def age(self) -> T.seconds.py:
        if self.is_cached:
            return (dates.now() - self.last_modified).seconds
        return 0

    @property
    def display_age(self) -> T.text.py:
        """"""
        return T.seconds.hum(self.age)

    def to_dict(self) -> Dict[str, Any]:
        """"""
        return {
            "key": self.key,
            "age": self.age,
            "value": self.value,
            "display_age": self.age,
            "last_modified": self.last_modified,
            "is_cached": self.is_cached,
            "debug": self.debug,
        }


class Cache(SerializableObject):
    """
    An abstract Redis Cache object to inherit from.
    ```
    from dada.models.file import File
    from dada.models.cache import Cache

    class FileCache(Cache):

        def work(self, file_id):
            f = File.get(file_id)
            return f

    file_cache = FileCache(key_prefix='file_cache')
    f = file_cache.get(file_id=1)
    print(f.to_dict())

    print(f.value.to_dict())
    file_cache.invalidate(file_id=1)
    ...
    ```
    """

    __module__ = "dada.models.cache"
    __gldb_type__ = "cache"

    rdsconn = rdsconn
    expiration = 84600  # 1 day
    key_prefix = None
    debug = False
    defaults = {}

    def __init__(
        self,
        rdsconn=rdsconn,
        key_prefix: str = "cache",
        expiration: int = 84600,
        debug=False,
        **kwargs,
    ):
        self.rdsconn = rdsconn or self.rdsconn
        self.key_prefix = key_prefix or self.key_prefix or self.object_name
        self.expiration = expiration or self.expiration
        self.debug = self.debug or debug
        self.defaults.update(kwargs)

    @property
    def expiration_human(self) -> T.text.py:
        """"""
        return T.seconds.hum(self.expiration)

    def to_dict(self) -> Dict[str, Any]:
        return {
            "name": self.object_name,
            "info": self.object_info,
            "key_root": self.key_root,
            "key_prefix": self.key_prefix,
            "key_base": f"{self.key_root}:{self.key_prefix}",
            "expiration": self.expiration,
            "expiration_human": self.expiration_human,
        }

    # ////////////////////////////////////////////////
    # Methods to Implement
    # ////////////////////////////////////////////////

    def fetch(self, *args, **kw):
        """
        The function for doing the work we'd like to avoid repeating
        """
        raise NotImplemented

    # ////////////////////////////////////////////////
    # Format the cache key from the function signature
    # ////////////////////////////////////////////////

    @property
    def key_root(self):
        """
        Overridable root rdsconn key.
        """
        return "gldb"

    def format_key(self, *args, **kw) -> str:
        """
        Formatting the fx like this allows us
        to modify it's behavior.
        """
        return self._format_key(*args, **kw)

    def _format_key(self, *args, **kw) -> str:
        """
        Format a unique key for redis. based on the function signatures parameters
        """
        # update kwargs with defaults
        kw = dict(list(self.defaults.items()) + list(kw.items()))
        hash_keys = []
        for a in sorted(args):
            hash_keys.append(str(a))
        for k, v in sorted(kw.items()):
            hash_keys.append(str(v))
        hash_str = md5("".join([k for k in hash_keys if k]).encode("utf-8")).hexdigest()
        return "{}:{}:{}".format(self.key_root, self.key_prefix, hash_str)

    # ////////////////////////////////////////////////
    # Serialization protocol: Pickle
    # ////////////////////////////////////////////////

    def dump(self, o: Any, **kwargs) -> Any:
        """
        The function for deserializing the string
        returned from redis
        """
        return serde.obj_to_pickle(o, **kwargs)

    def load(self, s: Any, **kwargs) -> Any:
        """
        The function for serializing the object
        returned from `get` to a string.
        """
        return serde.pickle_to_obj(s, **kwargs)

    # ////////////////////////////////////////////////
    # Caching Logic
    # ////////////////////////////////////////////////

    def exists(self, *args, **kw) -> True:
        """
        Check if the
        """
        return self.rdsconn.get(self.format_key(*args, **kw)) is not None

    def get(self, *args, **kw) -> CacheResponse:
        """
        The main get/cache function.
        """
        # combine keywords with defaults
        kw = dict(list(self.defaults.items()) + list(kw.items()))

        CACHE_LOGGER.debug(f"Fetching from cache {self.__gldb_type__}: {kw}")
        # get a custom expiration, fallback on default
        expiration = kw.pop("expiration", self.expiration)

        # format the key
        key = self.format_key(*args, **kw)

        # last modified key
        lm_key = "{}:lm".format(key)

        # in debug mode we never cache
        if not self.debug:
            obj = self.rdsconn.get(key)
        else:
            obj = None

        # if it doesn't exist, proceed with work
        if not obj:

            # not cached
            is_cached = False

            obj = self.fetch(*args, **kw)

            # if the worker returns None, break out
            if not obj:
                return CacheResponse(key, obj, None, False, self.debug)

            # set the object in redis at the specified
            # key with the specified expiration
            self.rdsconn.set(key, self.dump(obj), ex=expiration)
            last_modified = dates.now()

            # set the last modified time
            self.rdsconn.set(lm_key, last_modified.isoformat(), ex=expiration)

        else:
            # is cached
            is_cached = True

            # if it does exist, deserialize it.
            obj = self.load(obj)

            # get the cached last modified time
            last_modified = dates.from_string(self.rdsconn.get(lm_key))

        return CacheResponse(key, obj, last_modified, is_cached, self.debug)

    # ////////////////////////////////////////////////
    # Cache Invalidation
    # ////////////////////////////////////////////////

    def invalidate_key(self, key: T.text.py) -> None:
        """
        Remove a key from the cache via raw key name
        """
        self.rdsconn.delete(key)

    def invalidate(self, *args, **kw) -> None:
        """
        Remove a key from the cache via it's function signaure
        """
        self.rdsconn.delete(self.format_key(*args, **kw))

    @classmethod
    def flush(cls) -> None:
        """
        Flush this cache.
        """
        for k in cls.rdsconn.keys():
            if k.startswith(cls.key_prefix):
                cls.rdsconn.delete(k)
