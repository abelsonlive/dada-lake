from dada.config.models.core import join_fields

# ///////////////////////////////////////
# Default JOB Generation
# ///////////////////////////////////////

# DEFAULT JOBS
JOB_DEFAULTS = [
    {"name": f"hourly", "info": f"Trigger tasks on a monthly basis"},
    {"name": f"daily", "info": f"Trigger tasks on a daily basis"},
    {"name": f"monday", "info": f"Trigger tasks on Monday"},
    {"name": f"friday", "info": f"Trigger tasks on Friday"},
    {"name": f"weekly", "info": f"Trigger tasks weekly (on Monday)"},
    {
        "name": f"monthly",
        "info": f"Trigger tasks on a monthly basis (on the first day on the month)",
    },
]

# /////////////////
# DEFAULT JOB FIELDS
# ////////////////

JOB_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["job"],
    "accepts_file_types": ["all"],
}

JOB_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "param_def",
        "type": "param_def",
        "info": "The schema definition for the job input parameters",
    },
]

JOB_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "JOB", JOB_DEFAULTS_DEFAULT_FIELDS_INIT, JOB_DEFAULTS_DEFAULT_FIELD_PROPS
)
