from dada.config.models.core import join_fields

# ///////////////////////////////////////
# Default Edge Relationship Generation
# ///////////////////////////////////////

# DEFAULT Edge RELATIONSHIPS
EDGE_DEFAULTS = [
    {
        "name": "bundle_contains_files",
        "backref": "file_derived_from_bundle",
        "info": "Bundle Contents",
        "edge_type": "one_to_many",
        # from
        "from_entity_type": "file",
        "from_file_type": "bundle",
        "from_file_subtype": "all",
        # to
        "to_entity_type": "file",
        "to_file_type": "all",
        "to_file_subtype": "all",
    },
    {
        "name": "file_formats",
        "info": "Alternative formats of this file",
        "backref": "file_original",
        "edge_type": "one_to_many",
        # from
        "from_entity_type": "file",
        "from_file_type": "all",
        "from_file_subtype": "all",
        # to
        "to_entity_type": "file",
        "to_file_type": "all",
        "to_file_subtype": "all",
    },
    {
        "name": "track_contains_loops",
        "backref": "loop_derived_from_track",
        "info": "Loops Taken From Tracks",
        "edge_type": "one_to_many",
        # from
        "from_entity_type": "file",
        "from_file_type": "audio",
        "from_file_subtype": "track",
        # to
        "to_entity_type": "file",
        "to_file_type": "audio",
        "to_file_subtype": "loop",
    },
    {
        "name": "track_sampled_by",
        "backref": "track_samples_from",
        "info": "Track Sampling",
        "edge_type": "many_to_many",
        # from
        "from_entity_type": "file",
        "from_file_type": "audio",
        "from_file_subtype": "track",
        # to
        "to_entity_type": "file",
        "to_file_type": "audio",
        "to_file_subtype": "track",
    },
    {
        "name": "audio_similarity",
        "info": "A similarity score capturing the affinity between two audio files",
        "edge_type": "one_to_one",
        # from
        "from_entity_type": "file",
        "from_file_type": "audio",
        "from_file_subtype": "all",
        # to
        "to_entity_type": "file",
        "to_file_type": "audio",
        "to_file_subtype": "all",
        # attribute
        "attribute_type": "float",  # this allows us to have a an arbitrarily-typed graph attribute.
    },
    {
        "name": "folder_similarity",
        "edge_type": "one_to_one",
        # from
        "from_entity_type": "folder",
        # to
        "to_entity_type": "folder",
        # attribute
        "attribute_type": "float",  # this allows us to have a an arbitrarily-typed graph attribute.
    },
    {
        "name": "file_contains_fields",
        "backref": "fields_within_files",
        "edge_type": "many_to_many",
        # from
        "from_entity_type": "file",
        # to
        "to_entity_type": "field",
    },
]

EDGE_DEFAULTS_EDGE_NAMES = list(r["name"] for r in EDGE_DEFAULTS)

# /////////////////
# DEFAULT  FIELDS
# ////////////////

EDGE_DEFAULTS_DEFAULT_FIELD_PROPS = {}

EDGE_DEFAULTS_DEFAULT_FIELDS_INIT = []

# EDGE_DEFAULTS_DEFAULT_FIELDS = join_fields(
#     "doc", DOC_DEFAULTS_DEFAULT_FIELDS_INIT, DOC_DEFAULTS_DEFAULT_FIELD_PROPS
# )
EDGE_DEFAULTS_DEFAULT_FIELDS = []
