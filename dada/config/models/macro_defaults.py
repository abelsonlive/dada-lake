from dada.config.models.core import join_fields

# ///////////////////////////////////////
# Default MACRO Generation
# ///////////////////////////////////////

# DEFAULT MACROS
MACRO_DEFAULTS = [
    {
        "name": "file_audio_extract_id3_fields",
        "info": "Extract ID3 fields from audio files",
        "entity_type": "file",
        "action": "transform",
        "accepts_file_types": ["audio"],
        "accepts_file_subtype": ["all"],
        "fields": {
            "param_def": {
                "file_id": {
                    "name": "File ID",
                    "info": "The file id to extract fields from",
                    "type": "file_id",
                },
            }
        },
    },
    {
        "name": "file_video_extract_ffp_fields",
        "info": "Extract meta fields from video files via ffprobe",
        "entity_type": "file",
        "action": "transform",
        "accepts_file_types": ["video"],
        "accepts_file_subtype": ["all"],
        "fields": {
            "param_def": {
                "file_id": {
                    "name": "File ID",
                    "info": "The file id to extract fields from",
                    "type": "file_id",
                },
            }
        },
    },
    {
        "name": "file_image_extract_exif_fields",
        "info": "Extract exif fields from image fiels via PIL",
        "entity_type": "file",
        "action": "transform",
        "accepts_file_types": ["image"],
        "accepts_file_subtypes": ["all"],
        "fields": {
            "param_def": {
                "file_id": {
                    "name": "File ID",
                    "info": "The file id to extract fields from",
                    "type": "file_id",
                },
            }
        },
    },
    {
        "name": "sql_file_field_denorm",
        "info": "Create a denormalized table of files -> fields",
        "entity_type": "sql",
        "action": "transform",
        "fields": {
            "param_def": {
                "file_type": {
                    "name": "File Type",
                    "info": "The file type to create a view for",
                    "type": "file_type",
                },
                "file_subtype": {
                    "name": "File SubType",
                    "info": "The file subtype to create a view for",
                    "type": "file_subtype",
                },
            }
        },
    },
]


# /////////////////
# DEFAULT MACRO FIELDS
# ////////////////

MACRO_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["macro"],
    "accepts_file_types": ["all"],
}

MACRO_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "param_def",
        "type": "param_def",
        "info": "The schema definition for the macro input parameters",
    },
]

MACRO_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "macro", MACRO_DEFAULTS_DEFAULT_FIELDS_INIT, MACRO_DEFAULTS_DEFAULT_FIELD_PROPS
)
