from dada.config.models.core import join_fields

TASK_JOB_DEFAULTS = [
    {"job": "job-hourly", "task": "task-sql_create_views", "fields": {"param_val": {}}}
]


# /////////////////
# DEFAULT TASK_JOB FIELDS
# ////////////////

TASK_JOB_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["task_job"],
}

TASK_JOB_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "param_val",
        "type": "param_val",
        "info": "The validated input parameters to pass to the asscociated macro when running this task",
    },
]

TASK_JOB_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "task_job",
    TASK_JOB_DEFAULTS_DEFAULT_FIELDS_INIT,
    TASK_JOB_DEFAULTS_DEFAULT_FIELD_PROPS,
)
