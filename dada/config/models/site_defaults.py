from dada.config.models.core import join_fields

# ///////////////////////////////////////
# Default SITE Generation
# ///////////////////////////////////////

# DEFAULT SITES
SITE_DEFAULTS = [
    {
        "name": "fifteen-pm",
        "info": "fifteen.pm releases",
        "fields": {
            "site_git_repo": "https://github.com/gltd/fifteen.pm.git",
            "site_git_branch": "master",
        },
    },
]


# /////////////////
# DEFAULT SITE FIELDS
# ////////////////

SITE_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["app"],
}

SITE_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "prod_s3_bucket",
        "type": "s3_url",
        "info": "The path to the docker image to run",
    },
    {
        "name": "dev_s3_bucket",
        "type": "s3_url",
        "info": "The path to the docker image to run",
    },
    {
        "name": "prod_cloudfront_distribution",
        "type": "cloudfront_distribution",
        "info": "The path to the docker image to run",
    },
    {
        "name": "dev_cloudfront_distribution",
        "type": "cloudfront_distribution",
        "info": "The path to the docker image to run",
    },
    {
        "name": "git_repo",
        "type": "git_repo",
        "info": "The git repository for the web site",
    },
    {
        "name": "git_branch",
        "type": "git_branch",
        "info": "The git repository for the web site",
    },
]

SITE_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "site", SITE_DEFAULTS_DEFAULT_FIELDS_INIT, SITE_DEFAULTS_DEFAULT_FIELD_PROPS
)
