from dada.config.models.core import join_fields

# /////////////////
# DEFAULT DATA FIELDS
# ////////////////

DATA_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["file"],
    "accepts_file_types": ["doc"],
}

DATA_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "n_rows",
        "type": "int",
        "info": "The number of rows in the dataset",
    },
    {
        "name": "n_cols",
        "type": "int",
        "info": "The number of columns in the dataset",
    },
    {
        "name": "gldb_schema",
        "type": "json",
        "info": "The inferred gldb schema",
    },
    {
        "name": "athena_table",
        "type": "text",
        "info": "The name of this dataset in the athena data catalog",
    },
]

DATA_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "data", DATA_DEFAULTS_DEFAULT_FIELDS_INIT, DATA_DEFAULTS_DEFAULT_FIELD_PROPS
)
