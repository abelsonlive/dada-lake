from dada.config.models.core import join_fields

# ////////////////////////////
# Default Macro Task Associations
# ////////////////////////////

MACRO_TASK_DEFAULTS = [
    {
        "macro": "file_audio_extract_id3_fields",
        "task": "file_extract_fields",
        "position": 1,
        "fields": {"param_val": {}},
    },
    {
        "macro": "file_video_extract_ffp_fields",
        "task": "file_extract_fields",
        "position": 2,
        "fields": {"param_val": {}},
    },
    {
        "macro": "file_image_extract_exif_fields",
        "task": "file_extract_fields",
        "position": 3,
        "fields": {"param_val": {}},
    },
    {
        "macro": "sql_file_field_denorm",
        "task": "sql_create_views",
        "position": 1,
        "fields": {"param_val": {}},
    },
]


# /////////////////
# DEFAULT MACRO_TASK FIELDS
# ////////////////

MACRO_TASK_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["macro_task"],
    "accepts_file_types": ["all"],
}

MACRO_TASK_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "param_val",
        "type": "param_val",
        "info": "The validated input parameters to pass to the asscociated macro when running this task",
    },
]

MACRO_TASK_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "macro_task",
    MACRO_TASK_DEFAULTS_DEFAULT_FIELDS_INIT,
    MACRO_TASK_DEFAULTS_DEFAULT_FIELD_PROPS,
)
