from dada.config.models.core import join_fields

# /////////////////
# DEFAULT TASKS
# ////////////////
TASK_DEFAULTS = [
    {
        "name": "file_extract_fields",
        "info": "Default field extraction for all files",
        "fields": {
            "param_def": {
                "file_id": {
                    "name": "File ID",
                    "info": "The file id to extract fields from",
                    "type": "file_id",
                },
            }
        },
    },
    {
        "name": "sql_create_views",
        "info": "Create Internal SQL Views",
        "fields": {
            "param_def": {
                "materialized": {
                    "name": "Materialization",
                    "info": "The materilizaiton for the tables (view, table, mat_view)",
                    "type": "text",
                    "default": "table",
                }
            }
        },
    },
]


# /////////////////
# DEFAULT TASK FIELDS
# ////////////////

TASK_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["task"],
    "accepts_file_types": ["all"],
}

TASK_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "param_def",
        "type": "param_def",
        "info": "The schema definition for the task input parameters",
    }
]

TASK_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "task", TASK_DEFAULTS_DEFAULT_FIELDS_INIT, TASK_DEFAULTS_DEFAULT_FIELD_PROPS
)
