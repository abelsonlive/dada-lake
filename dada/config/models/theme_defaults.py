from dada.config.models.core import join_fields

# /////////////////
# DEFAULT THEME FIELDS
# ////////////////

SITE_THEME_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["site_theme"],
}

SITE_THEME_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "css",
        "type": "text",
        "info": "The compiled CSS to use for this site. ",
    },
]

SITE_THEME_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "site_theme",
    SITE_THEME_DEFAULTS_DEFAULT_FIELDS_INIT,
    SITE_THEME_DEFAULTS_DEFAULT_FIELD_PROPS,
)
