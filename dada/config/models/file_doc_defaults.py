from dada.config.models.core import join_fields

# /////////////////
# DEFAULT DOC FIELDS
# ////////////////

DOC_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["file"],
    "accepts_file_types": ["doc"],
}

DOC_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "summary",
        "type": "text",
        "info": "The document summary",
        "is_searchable": True,
    },
]

DOC_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "doc", DOC_DEFAULTS_DEFAULT_FIELDS_INIT, DOC_DEFAULTS_DEFAULT_FIELD_PROPS
)
