# ///////////////////////////////////////
# Default HOOK Generation
# ///////////////////////////////////////

from dada.config.models.core import join_fields

from dada.config.constants.hook_defaults import (
    HOOK_DEFAULTS_ENTITY_TYPES,
    HOOK_DEFAULTS_ACTIONS,
)

# DEFAULT HOOK
HOOK_DEFAULTS = [
    {
        "name": f"{entity}_{action}",
        "info": f"Trigger actions when a `{entity}` is `{action}`",
        "entity_type": entity,
        "action": action,
    }
    for entity in HOOK_DEFAULTS_ENTITY_TYPES
    for action in HOOK_DEFAULTS_ACTIONS
]

HOOK_DEFAULTS_DEFAULT_FIELDS = []
