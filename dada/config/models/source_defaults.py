from dada.config.models.core import join_fields

# ///////////////////////////////////////
# Default Source Generation
# ///////////////////////////////////////

# DEFAULT Sources
SOURCE_DEFAULTS = [
    {
        "name": "soundcloud",
        "info": "Soundcloud is a platform",
        "accepts_entity_types": ["file"],
        "transmits_entity_types": ["file"],
        "transmits_file_types": ["image", "audio", "data"],
        "accepts_file_types": ["image", "audio", "data"],
        "fields": {"credentials": {}},
    },
    {
        "name": "youtube",
        "info": "youtube is a platform",
        "accepts_entity_types": ["file"],
        "transmits_entity_types": ["file"],
        "transmits_file_types": ["image", "audio", "data"],
        "accepts_file_types": ["image", "audio", "data"],
        "fields": {"credentials": {}},
    },
    {
        "name": "bandcamp",
        "info": "bandcamp is a platform",
        "accepts_entity_types": ["file"],
        "transmits_entity_types": ["file"],
        "transmits_file_types": ["image", "audio", "data"],
        "accepts_file_types": ["image", "audio", "data"],
        "fields": {"credentials": {}},
    },
    {
        "name": "slack",
        "info": "slack is a platform",
        "accepts_entity_types": ["file"],
        "transmits_entity_types": ["file"],
        "transmits_file_types": ["image", "audio", "data", "doc", "video"],
        "accepts_file_types": ["image", "audio", "data", "doc", "video"],
        "fields": {"credentials": {}},
    },
    {
        "name": "notion",
        "info": "notion is a platform",
        "transmits_file_types": ["image", "audio", "data", "doc", "video"],
        "accepts_file_types": ["image", "audio", "data", "doc", "video"],
        "fields": {"credentials": {}},
    },
    {
        "name": "aws",
        "info": "aws is a platform",
        "transmits_file_types": ["image", "audio", "data", "doc", "video"],
        "accepts_file_types": ["image", "audio", "data", "doc", "video"],
        "fields": {"credentials": {}},
    },
]


# /////////////////
# DEFAULT SOURCE FIELDS
# ////////////////

SOURCE_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["source"],
}

SOURCE_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "credentials",
        "type": "json",
        "info": "Credentials for accesing this source",
    },
]

SOURCE_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "source", SOURCE_DEFAULTS_DEFAULT_FIELDS_INIT, SOURCE_DEFAULTS_DEFAULT_FIELD_PROPS
)
