from dada.config.models.core import join_fields

# ///////////////////////////////////////
# Default APP Generation
# ///////////////////////////////////////

# DEFAULT APPS
APP_DEFAULTS = [
    {
        "name": "globally-ltd",
        "info": "globally.ltd Streaming application",
        "fields": {
            "app_git_repo": "https://github.com/gltd/globally.ltd.git",
            "app_git_branch": "master",
        },
    },
    {
        "name": "gldb",
        "info": "globally.ltd's Operation System",
        "fields": {
            "app_git_repo": "https://github.com/gltd/gldb.ltd.git",
            "app_git_branch": "master",
        },
    },
]


# /////////////////
# DEFAULT APP FIELDS
# ////////////////

APP_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["app"],
}

APP_DEFAULTS_DEFAULT_FIELDS_INIT = [
    {
        "name": "docker_image",
        "type": "docker_image",
        "info": "The path to the docker image to run",
    },
    {
        "name": "git_repo",
        "type": "text",
        "info": "The git repository for the applicaiton",
    },
    {
        "name": "dns",
        "type": "text",
        "info": "The dns entry for this application",
    },
]

APP_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "app", APP_DEFAULTS_DEFAULT_FIELDS_INIT, APP_DEFAULTS_DEFAULT_FIELD_PROPS
)
