MACRO_DEFAULTS_ACTIONS = ["extract", "transform", "load"]


MACRO_DEFAULTS_ENTITY_TYPES = [
    "file",
    "folder",
    "desktop",
    "user",
    "source",
    "site",
    "app",
    "tag",
    "sql",
]
