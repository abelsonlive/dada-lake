HOOK_DEFAULTS_ENTITY_TYPES = [
    "source_file",
    "file",
    "folder",
    "file_folder",
    "folder_desktop",
    "desktop",
    "tag",
    "job",
    "user",
    "task",
    "macro",
    "macro_task",
    "task_job",
    "user_source",
    "app",
    "site",
    "app_theme",
    "site_theme",
    "tag_theme",
    "edge_theme",
    "file_theme",
    "field_theme",
    "folder_theme",
    "desktop_theme",
    "user_theme",
    "hook_theme",
    "macro_theme",
    "task_theme",
    "job_theme",
    "source_theme",
]

HOOK_DEFAULTS_ACTIONS = [
    "searched",
    "created",
    "updated",
    "deleted",
    "streamed",
    "downloaded"
    # ... MORE HERE!
]
