IMAGE_DEFAULTS_VALID_EXT_MIMETYPE = {
    "jpg": "image/jpeg",
    "jpeg": "image/jpeg",
    "png": "image/png",
    "bmp": "image/bmp",
    "gif": "image/gif",
    "tif": "image/tiff",
    "tiff": "image/tiff",
    "eps": "application/postscript",
    "svg": "image/svg+xml",
    "ico": "image/vnd.microsoft.icon",
    "webp": "image/webp",
}

# default image metadata extraction
IMAGE_DEFAULTS_DEFAULT_METADATA_EXTRACTION = "exif"
