SOURCE_DEFAULTS_TRANSMITS_ENTITY_TYPES = [
    "file",
    "bundle",
    "folder",
    "desktop",
    "user",
    "macro",
    "task",
    "job",
    "tag",
    "theme",
    "source",
]

SOURCE_DEFAULTS_ACCEPTS_ENTITY_TYPES = [
    "file",
    "bundle",
    "folder",
    "desktop",
    "user",
    "macro",
    "task",
    "job",
    "tag",
    "theme",
    "source",
]
