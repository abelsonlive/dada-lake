# /////////////////
# FILE EXTENSIONS
# /////////////////

VIDEO_DEFAULTS_VALID_EXT_MIMETYPE = {
    "flv": "video/x-flv",
    "mp4": "video/mp4",
    "m3u8": "application/x-mpegurl",
    "ts": "video/mp2t",
    "3gp": "video/3gpp",
    "mov": "video/quicktime",
    "avi": "video/x-msvideo",
    "wmv": "video/x-ms-wmv",
    "mpeg": "video/mpeg",
}

# metadata extraction

VIDEO_DEFAULTS_DEFAULT_METADATA_EXTRACTION = "ffp"
