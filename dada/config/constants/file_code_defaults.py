# /////////////////
# Code FILE EXTENSIONS
# /////////////////

CODE_DEFAULTS_VALID_EXT_MIMETYPE = {
    "py": "text/x-python",
    "js": "text/javascript",
    "css": "text/css",
    "html": "application/html",
    "xhtml": "application/xhtml+xml",
    "j2": "application/x-jinja",
    "r": "application/x-rstats",
    "sh": "application/x-sh",
    "zsh": "application/x-zsh",
}
