# /////////////////
# DATA FILE EXTENSIONS
# /////////////////

DATA_DEFAULTS_VALID_EXT_MIMETYPE = {
    "csv": "text/csv",
    "parquet": "text/x-parquet",
    "json": "application/json",
    "jsonl": "application/json+ld",
    "jsonld": "application/json+ld",
    "xml": "application/xml",
    "yaml": "application/x-yaml",
    "yml": "application/x-yaml",
}

# TODO: IMPLEMENT THIS LATER
