MODEL_DEFAULTS_VALID_EXT_MIMETYPE = {
    "gltf": "model/gltf",
    "glb": "model/gltf",
    "glk": "model/glk",
    "stl": "application/octect-stream",
    "obj": "application/octect-stream",
    "fbx": "application/octect-stream",
}
