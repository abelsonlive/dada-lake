EDGE_DEFAULTS_EDGE_TYPES = [
    "one_to_many",  # one_to_many # eg and archive contains x files. a song contains y samples. a video contains z loops.
    "one_to_one",  # x file has an affinity to y file
    "many_to_many",  # x files samples from y files
]

EDGE_DEFAULTS_ENTITY_TYPES = [
    "edge",
    "field",
    "file",
    "file_folder",
    "folder_desktop",
    "folder",
    "graph",
    "hook",
    "job",
    "desktop",
    "macro_task",
    "macro",
    "source",
    "tag",
    "task_hook",
    "task",
    "task_job",
    "user",
    "app",
    "site",
    "file_app",
    "file_site",
    "source_file",
]
