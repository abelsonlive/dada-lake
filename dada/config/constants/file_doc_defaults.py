# /////////////////
# Document FILE EXTENSIONS
# /////////////////

DOC_DEFAULTS_VALID_EXT_MIMETYPE = {
    "pdf": "application/pdf",
    "txt": "text/plain",
    "text": "text/plain",
    "md": "text/markdown; charset=UTF-8",
    "markdown": "text/markdown; charset=UTF-8",
    "ppt": "application/vnd.ms-powerpoint",
    "pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "doc": "application/msword",
    "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "rtf": "application/rtf",
}


# TODO: IMPLEMENT THIS LATER
