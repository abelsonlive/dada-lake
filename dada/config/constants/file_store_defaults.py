# local directory (where we store files in local mode)
FILE_STORE_LOCAL_DIR = "~/.gldb"
FILE_STORE_LOCAL_AUDITS = False  # whether or not to audit records in local mode
FILE_STORE_LOCAL_VERSIONS = False  # whether or not to version files in local mode

#
FILE_STORE_PUBLIC_ENDPOINT = "https://assets.gl"
