import os

ENV = os.getenv("DADA_ENV", "dev")
if not ENV:
    raise RuntimeError("No DADA_ENV supplied.")

from dada.config.core import *

if ENV == "prod":
    from dada.config.prod import *

if ENV == "dev":
    from dada.config.dev import *

if ENV == "test":
    from dada.config.test import *

if ENV == "docker":
    from dada.config.docker import *

# constants


from dada.config.constants.field_defaults import *
from dada.config.constants.theme_defaults import *


from dada.config.constants.file_defaults import *
from dada.config.constants.file_bundle_defaults import *
from dada.config.constants.file_code_defaults import *
from dada.config.constants.file_audio_defaults import *
from dada.config.constants.file_data_defaults import *
from dada.config.constants.file_doc_defaults import *
from dada.config.constants.file_model_defaults import *
from dada.config.constants.file_image_defaults import *
from dada.config.constants.file_video_defaults import *
from dada.config.constants.file_store_defaults import *

from dada.config.constants.edge_defaults import *
from dada.config.constants.hook_defaults import *
from dada.config.constants.macro_defaults import *
from dada.config.constants.source_defaults import *
from dada.config.constants.tag_defaults import *
from dada.config.constants.task_defaults import *
from dada.config.constants.job_defaults import *

from dada.config.constants.app_defaults import *
from dada.config.constants.site_defaults import *

# model defaults
from dada.config.models.edge_defaults import *
from dada.config.models.theme_defaults import *
from dada.config.models.user_defaults import *
from dada.config.models.tag_defaults import *
from dada.config.models.field_defaults import *
from dada.config.models.source_defaults import *

from dada.config.models.desktop_defaults import *
from dada.config.models.folder_defaults import *
from dada.config.models.file_bundle_defaults import *
from dada.config.models.file_defaults import *
from dada.config.models.file_code_defaults import *
from dada.config.models.file_data_defaults import *
from dada.config.models.file_doc_defaults import *
from dada.config.models.file_audio_defaults import *
from dada.config.models.file_image_defaults import *
from dada.config.models.file_video_defaults import *
from dada.config.models.file_model_defaults import *

from dada.config.models.hook_defaults import *
from dada.config.models.macro_defaults import *
from dada.config.models.job_defaults import *
from dada.config.models.task_defaults import *
from dada.config.models.macro_task_defaults import *
from dada.config.models.task_job_defaults import *

from dada.config.models.app_defaults import *
from dada.config.models.site_defaults import *

# gen defaults
from dada.config.gen.desktop_defaults import *
from dada.config.gen.field_defaults import *
from dada.config.gen.folder_defaults import *

# ///////////////////////////////////////
# File extension / mimetype lookups
# ///////////////////////////////////////


FILE_VALID_TYPE_EXT_MIMETYPE = {
    "audio": AUDIO_DEFAULTS_VALID_EXT_MIMETYPE,
    "video": VIDEO_DEFAULTS_VALID_EXT_MIMETYPE,
    "image": IMAGE_DEFAULTS_VALID_EXT_MIMETYPE,
    "model": MODEL_DEFAULTS_VALID_EXT_MIMETYPE,
    "bundle": BUNDLE_DEFAULTS_VALID_EXT_MIMETYPE,
    "data": DATA_DEFAULTS_VALID_EXT_MIMETYPE,
    "code": CODE_DEFAULTS_VALID_EXT_MIMETYPE,
    "doc": DOC_DEFAULTS_VALID_EXT_MIMETYPE,
    "raw": {},
}


def _rev(d):
    return {v: k for k, v in d.items()}


FILE_VALID_TYPE_MIMETYPE_EXT = {
    "audio": _rev(AUDIO_DEFAULTS_VALID_EXT_MIMETYPE),
    "video": _rev(VIDEO_DEFAULTS_VALID_EXT_MIMETYPE),
    "image": _rev(IMAGE_DEFAULTS_VALID_EXT_MIMETYPE),
    "model": _rev(MODEL_DEFAULTS_VALID_EXT_MIMETYPE),
    "bundle": _rev(BUNDLE_DEFAULTS_VALID_EXT_MIMETYPE),
    "data": _rev(DATA_DEFAULTS_VALID_EXT_MIMETYPE),
    "code": _rev(CODE_DEFAULTS_VALID_EXT_MIMETYPE),
    "doc": _rev(DOC_DEFAULTS_VALID_EXT_MIMETYPE),
    "raw": _rev({}),
}

FILE_VALID_EXT_MIMETYPE = {
    kk: vv for k, v in FILE_VALID_TYPE_EXT_MIMETYPE.items() for kk, vv in v.items()
}

FILE_VALID_MIMETYPE_EXT = {
    kk: vv for k, v in FILE_VALID_TYPE_MIMETYPE_EXT.items() for kk, vv in v.items()
}

# ///////////////////////////////////////
# Default Fields by entity type / file type
# ///////////////////////////////////////

FIELD_TYPE_DEFAULTS = {
    "app": APP_DEFAULTS_DEFAULT_FIELDS,
    "file": {
        "audio": AUDIO_DEFAULTS_DEFAULT_FIELDS,
        "bundle": BUNDLE_DEFAULTS_DEFAULT_FIELDS,
        "code": CODE_DEFAULTS_DEFAULT_FIELDS,
        "doc": DOC_DEFAULTS_DEFAULT_FIELDS,
        "data": DATA_DEFAULTS_DEFAULT_FIELDS,
        "image": IMAGE_DEFAULTS_DEFAULT_FIELDS,
        "model": MODEL_DEFAULTS_DEFAULT_FIELDS,
        "video": VIDEO_DEFAULTS_DEFAULT_FIELDS,
        "raw": [],
    },
    "desktop": DESKTOP_DEFAULTS_DEFAULT_FIELDS,
    "edge": EDGE_DEFAULTS_DEFAULT_FIELDS,
    "folder": FOLDER_DEFAULTS_DEFAULT_FIELDS,
    "hook": HOOK_DEFAULTS_DEFAULT_FIELDS,
    "job": HOOK_DEFAULTS_DEFAULT_FIELDS,
    "macro": MACRO_DEFAULTS_DEFAULT_FIELDS,
    "macro_task": MACRO_TASK_DEFAULTS_DEFAULT_FIELDS,
    "site": SITE_DEFAULTS_DEFAULT_FIELDS,
    "source": SOURCE_DEFAULTS_DEFAULT_FIELDS,
    "tag": TAG_DEFAULTS_DEFAULT_FIELDS,
    "task": TASK_DEFAULTS_DEFAULT_FIELDS,
    "task_job": TASK_JOB_DEFAULTS_DEFAULT_FIELDS,
    # "site_theme": SITE_THEME_DEFAULTS_DEFAULT_FIELDS,
    "user": USER_DEFAULTS_DEFAULT_FIELDS,
}

# construct list of default fields
FIELD_DEFAULTS = []
for k, v in FIELD_TYPE_DEFAULTS.items():
    if isinstance(v, dict):
        for kk, vv in v.items():
            FIELD_DEFAULTS.extend(vv)
    else:
        FIELD_DEFAULTS.extend(v)

FIELD_DEFAULT_NAMES = [v["name"] for v in FIELD_DEFAULTS if "name" in v]
