import os
from dotenv import load_dotenv

load_dotenv()

# app

SECRET_KEY = os.getenv("DADA_SUPER_USER_SECRET_KEY", "dada123")
PORT = os.getenv("DADA_PORT", 3030)
HOST = os.getenv("DADA_HOST", "0.0.0.0")
HTTPS = os.getenv("DADA_HTTPS", "false").lower() == "true"
DEFAULT_BASE_URL = "http://localhost:3030"
if HTTPS:
    DEFAULT_BASE_URL = DEFAULT_BASE_URL.replace("http", "https")
BASE_URL = os.getenv("DADA_BASE_URL", DEFAULT_BASE_URL)
API_KEY_HEADER = os.getenv("DADA_API_KEY_HEADER", "X-DADA-API-KEY")
ENV_VAR_PREFIX = os.getenv("DADA_ENV_VAR_PREFIX", "dada")

# db
SQLALCHEMY_DATABASE_URI = os.getenv(
    "DADA_SQLALCHEMY_DATABASE_URI", "postgresql://localhost:5432/dada"
)
SQLALCHEMY_TRACK_MODIFICATIONS = False

# super user
SUPER_USER_NAME = os.getenv("DADA_SUPER_USER_NAME", "gltd")
SUPER_USER_EMAIL = os.getenv("DADA_SUPER_USER_EMAIL", "dev@globally.ltd")
SUPER_USER_PASSWORD = os.getenv("DADA_SUPER_USER_PASSWORD", "dev")
SUPER_USER_API_KEY = os.getenv("DADA_SUPER_USER_API_KEY", "dev")

# filesearch
FILE_SEARCH_RESULTS_PER_PAGE = os.getenv("DADA_FILE_SEARCH_RESULTS_PER_PAGE", 10)

# celery
CELERY_BROKER_URL = os.getenv("DADA_CELERY_BROKER_URL", "redis://localhost:6379")
CELERY_RESULT_BACKEND = os.getenv(
    "DADA_CELERY_RESULT_BACKEND", "redis://localhost:6379"
)
CELERY_ACCEPT_CONTENT = [os.getenv("DADA_CELERY_ACCEPT_CONTENT", "application/json")]
CELERY_RESULT_SERIALIZER = os.getenv("DADA_CELERY_RESULT_SERIALIZER", "json")
CELERY_TASK_SERIALIZER = os.getenv("DADA_CELERY_TASK_SERIALIZER", "json")

# redis cache
REDIS_CACHE_URL = os.getenv("DADA_REDIS_CACHE_URL", "redis://localhost:6379/")
REDIS_CACHE_DB = os.getenv("DADA_REDIS_CACHE_DB", "0")
REDIS_CACHE_KWARGS = os.getenv("DADA_REDIS_CACHE_KWARGS", {})
REDIS_CACHE_SERIALIZER = "pickle"  # TODO: implement something else?


# filestore
S3_BUCKET = os.getenv("DADA_S3_BUCKET", "dada.globally.ltd")
AWS_ACCESS_KEY_ID = os.getenv("DADA_AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = os.getenv("DADA_AWS_SECRET_ACCESS_KEY")
LOCAL_DIR = os.path.expanduser("~/.dada/local")

# temp directory
TEMPFILE_DIR = None  # override for docker environment


# api spec
APISPEC_SWAGGER_URL = os.getenv("DADA_APISPEC_SWAGGER_URL", "/spec/")
APISPEC_SWAGGER_UI_URL = os.getenv("DADA_APISPEC_SWAGGER_UI_URL", "/docs/")

# logging settings
LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "default": {"format": ("[%(asctime)s] <%(levelname)s> %(message)s")}
    },
    "datefmt": "%I:%M:%S",
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "INFO",
            "formatter": "default",
        }
    },
    "root": {"level": "INFO", "handlers": ["console"]},
}
