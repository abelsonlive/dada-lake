from dada.macros.core import BaseMacro


class FileTransfromMacro(BaseMacro):
    """
    A file Macro recieves a file, checks if the local path exists, if not it downloads it from s3, and then passes it to a transformation function
    """

    __entity_type__ = "file"
    __action__ = "transform"
    __param_def__ = {}
