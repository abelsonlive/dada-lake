from typing import Optional, Any

from dada.utils import path
from dada.types import T
from dada.types.base import SerializableObject
from dada.models.core import s3conn
from dada.macros.param import Parameters
from dada.config import settings


class BaseMacro(SerializableObject):
    """
    A macro is a simple class for running a function as a cli / api background job
    """

    __abstract__ = True
    __param_def__ = {}

    # access to api utilities
    T = T
    s3conn = s3conn

    def __init__(self):
        self.params = Parameters(**self.__param_def__)

    def do(self, **kwargs):
        raise NotImplementedError("You must implement a `do` method for a macro.")

    def run(self, **kwargs):
        return self.do(**self.params.validate(**kwargs))
