
# devving

dev:
	
	echo "Setting up dev environment"
	brew bundle
	make install 
	make db-recreate env=dev
	make db-upgrade env=dev
	make db-create-defaults env=dev


shell:

	flask shell

install:

	pip3 install -e .

lint:

	find dada/ -name '*.py' | xargs black

test:

	DADA_ENV=test pytest -p no:warnings

test-one:

	DADA_ENV=test pytest -p no:warnings $(t)


# db 
db-init:

	DADA_ENV='$(env)' flask db-init

db-migrate:

	DADA_ENV='$(env)' flask db migrate
	DADA_ENV='$(env)' make db-create-schema-diagram

db-upgrade:

	DADA_ENV='$(env)' flask db upgrade

db-create-defaults:

	DADA_ENV='$(env)' flask db-create-defaults

db-recreate:

	dropdb dada || true
	createdb dada
	DADA_ENV='$(env)' flask db upgrade

db-recreate-test:

	dropdb dada_test || true
	createdb dada_test
	DADA_ENV='test' flask db upgrade

db-create-schema-diagram:

	scripts/create-db-schema.sh

db-view-schema-diagram:

	open migrations/db-schema.pdf

# serving / background workers

serv-flask:

	DADA_ENV='$(env)' flask run -h 0.0.0.0 -p 3030

serv-gunicorn:

	DADA_ENV='$(env)' gunicorn dada.cli.admin:app -b 0.0.0.0:3030 -w 4 -k gevent -t 300

celery-helper:

	DADA_ENV='$(env)' celery -A scripts.celery_helper.celery  worker -l info


# swagger codegen

gen-js-client:

	rm -rf clients/js;
	swagger-codegen generate -i http://localhost:3030/spec.json -l javascript -o clients/js/   --git-user-id=gltd   --git-repo-id=gldb

gen-python-client:

	rm -rf clients/python;
	swagger-codegen generate -i http://localhost:3030/spec.json -l python -o clients/python
